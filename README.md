# Importante
El archivo `Apuntes_Curso.md` es el documento de trabajo con las notas e imágenes tomadas del Curso.

También hay un archivo `index.html`, que traslada todo el contenido de los apuntes a un soporte más amigable para leer y revisar.
