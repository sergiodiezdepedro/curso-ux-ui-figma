Listado de preguntas:

¿Cómo doy a conocer mi trabajo?

¿Cómo construir un portafolio de UX/UI? ¿Qué muestro?

Si quiero dedicarme a trabajar a tiempo completo en Diseño UX. ¿Cómo influye tener un perfil orientado a soporte técnico y programación? ¿Optan por contratar gente con estudios más relacionados con diseño/humanidades?

¿Cómo puedo accede al ámbito laboral, si no tengo experiencia y es uno de los requisitos?

¿Cómo puedo adquirir más práctica (no importa si no es remunerada) para comenzar a trabajar?

¿Cómo funciona el cargo de UX? ¿Hay un grupo de trabajo o es un trabajo solitario?

¿Es necesario seguir capacitándose en diseño o Figma para poder obtener trabajo?

¿Cómo funciona el cargo de UX? ¿Hay un grupo de trabajo o es un trabajo solitario?

¿Qué recomendaciones podrías hacerle a un programador que quiere mejorar su conocimiento en UI y UX?

¿Qué páginas recomiendas navegar para la búsqueda de ofertas laborales o dónde conseguir trabajo?

¿Qué empresas recomiendas para postularse?

-----

**¿Cómo doy a conocer mi trabajo?**

Podrás dar a conocer tu trabajo con un portfolio o compartiéndolo en una red social. Por ejemplo, puedes hacerlo en [Behance](https://www.behance.net/), es la mayor plataforma para portfolios de diseñadores. [Dribbble](https://dribbble.com/) también es una opción viable, o también, creando tu propia página web con tus proyectos. Podrás encontrar varios de estos ejemplos en https://www.awwwards.com/, buscando “UX/UI Designer” o “Portfolio”.


**¿Cómo construir un portafolio de UX/UI? ¿Qué muestro?**

En un portfolio de diseño UX/UI es muy importante mostrar el proceso. No basta con mostrar el resultado de las pantallas que diseñamos ya sea para desktop o una aplicación móvil, sino ser capaces de narrar y mostrar todo lo que atravesamos para llegar hasta ahí. Nuestro punto de partida e hipótesis, nuestra investigación y hallazgos, las técnicas y herramientas que utilizamos, el por qué hicimos uso de dichas herramientas y técnicas, los desafíos y pasos en la creación de la web por ejemplo, los resultados de las pruebas de usabilidad o la validación que hicimos de nuestra creación, etc. Este es un tema muy extenso.

**Si quiero dedicarme a trabajar a tiempo completo en Diseño UX. ¿Cómo influye tener un perfil orientado a soporte técnico y programación? ¿Optan por contratar gente con estudios más relacionados con diseño/humanidades?**

Ambos perfiles son aptos y valiosos para conformar un equipo de UX. La variedad es lo que hace a un equipo valioso.


**¿Cómo puedo acceder al ámbito laboral, si no tengo experiencia y es uno de los requisitos?**

Lo que adquiriste en este curso es experiencia profesional, es decir, experiencia ejerciendo una profesión, la falta de experiencia laboral es normal y esperable de un perfil junior. Lo mejor en estos casos es seguir desarrollando proyectos y aprendiendo más.


**¿Cómo puedo adquirir más práctica (no importa si no es remunerada) para comenzar a trabajar?**

Hay varias opciones para esto. Se puede crear un producto digital como el que crearás en este curso, atravesando por todo un proceso de diseño. Así como harás el de la problemática del *Reciclaje* puedes hacerlo con otro tema totalmente distinto, mismo proceso, distinto resultado.

Otra opción es rediseñar algún servicio, página web o aplicación móvil poco conocida y que tenga muchas cosas a mejorar. Algo de lo que no soy partidario ni recomiendo hacer es rediseñar una aplicación o página web conocida. Sea Amazon, Steam, Youtube, Twitter, Airbnb, etc.

Esto no es una buena idea ya que esas plataformas tienen cientos de diseñadores por detrás y todas sus decisiones fueron tomadas en base a algo, ya sea decisiones de negocio o hallazgos de una investigación que realizaron los equipos de UX.

Para rediseñar o crear algo desde cero es una buena opción hacerlo en equipo, nuestra comunidad en Discord es una buena opción para encontrar a alguien que quiera hacer algo en conjunto.

Una última opción viable e interesante es ofrecerle nuestros servicios a una ONG, ya sea mejorar algo interno de ellos, crearles la página web o colaborar con cierta tarea relacionada a UX/UI. Muchas ONGs buscan ayuda y en Linkedin podrás encontrar info sobre ellas.

**¿Es necesario seguir capacitándose en diseño o Figma para poder obtener trabajo?**

En Figma no. En diseño sí, este curso de 22 h por más que parezca completo, podría durar 80 h. Lo mejor para seguir capacitándose es mediante libros, otros cursos, o personalmente recomiendo los cursos de [Interaction Design Foundation](https://www.interaction-design.org/).

A partir de ahora qué aprender dependerá de qué te interese. Ya aprendiste todas las disciplinas y temas que conforman el diseño UX. Quizás te interesa el área de investigación, o la comunicación y UX Writing, o quizás el diseño UI. A mí personalmente me gusta todo, por eso me considero un diseñador de producto (Product Designer). Trabajo desde la concepción de la idea, entendiendo el negocio y a los usuarios, la investigación, la interfaz, comunicación y continuar hasta después de la salida al mercado.

**¿Cómo funciona el cargo de UX? ¿Hay un grupo de trabajo o es un trabajo solitario?**

Es un rol que se trabaja en equipo, con un equipo de negocio y uno de desarrolladores. A menos que trabajemos como independientes (que en gran porcentaje también se trabaja en equipo), en el 99% de los casos vamos a trabajar junto con otros diseñadores, researchers, desarrolladores, etc.

**¿Qué recomendaciones podrías hacerle a un programador que quiere mejorar su conocimiento en UI y UX?**

Ya habiendo aprendido sobre todo el proceso de UX, a un desarrollador le recomendaría 3 cosas.

Primero: aprender sobre accesibilidad y cómo hacer los desarrollos y diseños más accesibles.

Segundo: profundizar sobre diseño visual y UI. Si bien en este curso aprendiste, lo mejor es seguir profundizando y trabajar con un `design system`, asi harás que tus diseños sean más estéticos y acompañen mejor a la marca. El diseño suele ser el talón de aquiles en los desarrolladores, ya que tienen un background distinto al de un diseñador.

Tercero: un pilar importante y que a lo largo de los años he detectado en proyectos de desarrolladores es que carecen de una comunicación y *storytelling* correcto. El contenido es de lo más importante en un producto digital.

Profundizar en estos 3 temas hará que tus desarrollos tengan una mejora enorme.

**Qué páginas recomiendas navegar para la búsqueda de ofertas laborales o dónde conseguir trabajo?**

Páginas para buscar empleo recomiendo principalmente dos. Linkedin e Indeed. En ambas hay una bolsa de trabajo enorme para puestos de experiencia de usuario y las dos son internacionales.

**¿Que empresas recomiendas para postularse?**

A la hora de buscar trabajo como junior, lo mejor es buscar empresas grandes. Por ejemplo MercadoLibre, Accenture, Globant, entre otras. Cuanto más grande sea la empresa, más estructura y jerarquía tienen, por lo que es más fácil ser contratado ya que tienen personas que pueden capacitar y acompañar a diseñadores juniors. Cuanto más pequeña sea la empresa más difícil será, en todo esto siempre está el *depende* o el *no aplica en todos los casos*, pero una empresa pequeña quizas no tiene la estructura suficiente para poder tener un equipo con varios tipos de seniority y equipos interdisciplinarios.