# Figma

## Atajos de teclado

- R: crear Rectángulo. R+ Mayúsculas crea un cuadrado.
- L: crear línea. L + Mayúsculas crea línea con angulos fijos de inclinación.
- O: crear elipse. O + Mayúscular crea un círculo.
- T: crear texto.
- F: crear Frames (Marcos).
- H: herramienta de Mano. Presionando ratón + Barra Espaciadora también habilita la Mano.
- V: herramienta de Selección/Mover.
- Elemento seleccionado + número cambia la opacidad.
- Zoom to Fit: Mayúsculas + 1.
- Zoom al 100%: Mayúsculas + 0.
- Seleccionar más de 1 elemento: seleccionar el deseado con Clic + Mayúsculas.
- Comando +  G: crear grupo de elementos seleccionados.
- Comando + Mayúsculas + G: desagrupa.
- I en elemento seleccionado: aparece un cuentagotas para elegir color para cambiar el del elemento.
- Comando + D: duplica el elemento seleccionado.
- Clic + Arrastrar + Option: duplica el elemento.
- Option + H, Option + V: alinear elementos en los ejes horizontal y vertical respectivamente.
- **Mayúsculas + A: Crear auto layout**

## Plugins:

- **Able** - Para evaluar la accesibilidad de los colores.
- **Blush** - ilustraciones gratuitas de todo tipo.
- **Brands Colors** - ¿Quieres saber qué colores utiliza WhatsApp? ¿Linkedln? 
- **Color Palettes** - Hermosas paletas de colores para tus diseños.
- **Feather Icons** - Librería de iconos gratuitos.
- **Figmiro** - ¿Utilizas Miro y Figma? Sincroniza las pantallas con este mismo plugin.
- **Iconify** - Nunca son suficientes los iconos y este plugin sabe eso.
- **Isometric** - Selecciona un elemento y colocalo en un ángulo que se haga lucir.
- **Material Design Icons** - Un must have si diseñas aplicaciones para Android.
- **Tiny Image Compressor** - Si quieres exportar tus diseños en PDF este plugin te lo permite.
- **UI Faces** - No pierdas tiempo buscando imágenes de caras, genéralas de forma sencilla.
- **Unsplash** - Imágenes de alta calidad gratuitas sin derecho de autor.
- **Beautiful Shadows** - Aplicar sombras personalizadas a los elementos.