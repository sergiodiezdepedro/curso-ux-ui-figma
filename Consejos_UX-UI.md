# Consejos UX/UI

- No todos los usuarios tienen 25 años y una visión perfecta.
- La identidad de marca es muy importante.
- Siempre diseñar los blank states, los loading, los error states.
- Diseñar el camino feliz es fácil, lo díficil es diseñar cuando las cosas salen mal.
- Ten en cuenta la tecnología del proyecto.
- Sin UX hay peligro.
- No todos los usuarios tienen iPhones...
- Ten en cuenta los márgenes y las *safe areas*.
- La jerarquía y la agrupación hacen magia.
- Estás diseñando productos, no publicidad.
- No es necesario mostrarle al usuario todo de una y en una misma pantalla.
- El proceso de diseño no termina en las pantallas que diseñamos, sino en los datos que recopilamos.
- Los *user testing* no validan que tu propuesta sea la mejor, sólo que no sea la peor.
- No se puede realizar siempre todo, hay *must* y *nice to have*.
- Siempre Mobile First.
- Tener consistencia acorta el esfuerzo y maximiza la usabilidad.
- Cuanto más simple sea tu diseño más aumentarás la probabilidad de conversión.
- Tener en cuenta cuántos recursos consume o consumiría tu diseño (en la vida real prácticamente nadie tiene 5G).
- Si no puedes explicar el problema a resolver de forma simple tu diseño tampoco lo será.
- Despues de diseñar un flujo revisa y elimina todos los elementos que no son estrictamente necesarios.
- Escuchar y prestar atención en lo que el cliente **necesita**, no en lo que quiere.
- Diseñar siempre en base a datos.
- A no preocuparse tanto y ser feliz, antes que lo perfecto, lo realizable.

