# Diseño UX: experiencia de usuario UX/UI + Figma 2024

## ¿Qué es la Experiencia de Usuario?

En conclusión, podemos definir la **Experiencia del Usuario** como la **sensación, sentimiento, respuesta emocional, valoración** y satisfacción del **usuario** respecto a un producto/servicio.

Resultado de la **interacción** con el producto y la interacción con su proveedor.

![Paraguas de la experiencia de usuario](img/paraguas.png)

## Diseño UX. UXD (User Experience Design)

El Diseño UX se apoya en técnicas de investigación y prototipado

A través de diferentes técnicas, el diseñador UX será capaz de diseñar el recorrido que la audiencia debe seguir para contratar un servicio o comprar un producto a través de una plataforma digital.

Para ello, **la fase de investigación es fundamental en el Diseño de Experiencia de Usuario**. Gracias a técnicas de research (Personas, Customer Journey, Benchmark, User Testing, etc.) los diseñadores UX analizan el público objetivo de una marca y diseñan la mejor experiencia.

El Diseño UX establece los wireframes de la futura web o app en base a una sólida investigación para asegurar un producto digital de calidad.

**Tareas del diseñador UX**:

- Análisis de la competencia
- Análisis del usuario
- Wireframing
- Prototipado
- Testing
- Coordinación con diseñadores UI
- Coordinación con desarrolladores web
- Análisis e Iteración

## Diseño UI. UID (User Interface Design)

El Diseño UI se compone de diseño visual y diseño de interacción

El diseño visual es el look and feel de un producto digital y tiene que transmitir la personalidad de la marca.

El diseño de interacción es la forma en la que el usuario interactúa con la web, la app, etc. Si el usuario pulsa un botón, ¿cómo sabe que lo ha pulsado? Por ejemplo, a través de un cambio de color o de tamaño, el usuario percibirá que la interacción se ha producido con éxito.

**Los diseñadores UI son los encargados de plasmar visualmente las decisiones de los diseñadores UX**. Deben ser expertos en el manejo de Sketch, el software más utilizado para diseño UI.

**Tareas del Diseñador UI**:

- Branding
- Diseño Gráfico
- Prototipado UI
- Implementación de la guía de estilo
- Diseño de Interacción
- Adaptación a todos los dispositivos
- Coordinación con desarrolladores web

## Diseño de Interacción. IxD Interaction Experience Design

Es una disciplina que nació de la necesidad de estudiar y facilitar las interacciones entre las personas y su ambiente.

El diseño de interacción (IxD) define la estructura y el comportamiento de sistemas interactivos.

Los diseñadores de interacción buscan crear relaciones significativas entre las personas y los productos o servicios que estos usan, desde ordenadores hasta dispositivos móviles, aparatos y más...

![IxD](img/ixd.png)

## Usabilidad

La Usabilidad o Calidad de Uso es un anglicismo que significa facilidad de uso y cuya definición formal se refiere al grado de **eficacia, eficiencia y satisfacción** con la que usuarios pueden lograr objetivos específicos.

![Usabilidad](img/usabilidad.png)

Eficiencia es lograr *x* objetivo/s con la menor cantidad de recursos, pasos, etc.

Eficacia es lograr *x* objetivo/s planteado/s.

La usabilidad en el campo del desarrollo web/mobile es la disciplina que estudia la forma de diseñar sitios web, apps, etc para que los usuarios puedan interactuar con ellos lo más fácil, cómoda e intuitivamente posible.

La mejor forma de crear un sitio web usable es realizando un diseño centrado en el usuario, diseñando por y para él, en contraposición a lo que podría ser un diseño centrado en la tecnología, o uno centrado en la creatividad u originalidad.

**¿Qué ventajas aporta un sitio web *usable*?**

- Mejora la experiencia y satisfacción de los usuarios.
- Logra una mayor comunicación y feedback con el usuario.
- Consigue más tráfico.
- Aumenta la duración de las visitas.  (En el buen sentido).
- Reduce el esfuerzo cognitivo.
- Etc, etc, etc.

**¿Cómo se hace una página web *usable*?**

- Conociendo a los usuarios.
- Teniendo objetivos claros.
- Un diseño simple y consistente.
- Navegación intuitiva.
- Accesibilidad.
- Jerarquía visual.
- Diseño responsivo.
- Etc.

## UX y Metologías Ágiles

UX y las Metodologías Ágiles tienen mucho en común.

- Sostienen el **desarrollo iterativo**.

- Hacen énfasis en **entregar valo**r a los usuarios.

- En ambos es sumamente importante el **trabajo en equipo**.

## Metodologías Ágiles

![Metodologías Ágiles](img/agiles.png)

![Metologías Ágiles iteraciones](img/agiles-2.png)

![Manifiesto Ágil](img/manifiesto-agil.png)

https://agilemanifesto.org/iso/es/manifesto.html

https://agilemanifesto.org/iso/es/principles.html

### Tipos de Metodologías Ágiles

**Metodologías enfocadas en generar innovación**:

- Design Sprint

- Design Thinking

**Metodologías para proyectos de desarrollo**:

- Agile

- Scrum

- Kanban

**Metodologías enfocadas en testear conceptos con clientes, mercados** (con MVP y otros):

- Lean Startup

- Design Sprint

## Design Thinking

Es un abordaje basado en métodos para la solución de problemas que utiliza técnicas y herramientas propias del pensamiento proyectual.

Es, sobre todo, un nuevo modelo cultural sobre la forma en que trabajamos para resolver problemas.

Es un proceso de innovación que se centra en encontrar ideas innovadoras y soluciones creativas que satisfagan las necesidades humanas. Esto significa que tu equipo está buscando soluciones creativas para problemas hiperespecíficos y complejos.

Segun la [Wikipedia](https://es.wikipedia.org/wiki/Pensamiento_de_dise%C3%B1o), es una manera de trabajar en grupo que maximiza la creatividad colectiva. Sucede que, a veces, personas que puedan ser muy brillantes a nivel individual, no rindan cuando las ponemos a trabajar en grupo. Design Thinking soluciona este problema definiendo pautas de cómo trabajar en grupo. Esto se hace aplicando cinco principios básicos:

1. Definiendo roles de trabajo (facilitator, expert, back-up man)
2. Delimitando fases de trabajo (Ver las cinco fases)
3. Definiendo pautas de comportamiento (ej. no criticar en ciertas fases)
4. Centrándose en el usuario (Ver human-centered design)
5. Iterando con frecuencia

![Design Thinking. Etapas](img/design-thinking.png)


![Design Thinking. Proceso](img/design-thinking-2.png)

Design Thinking es iterativo e incremental.

![Design Thinking iteración](img/design-thinking-3.png)

--------------

## Fase Empatizar

**UXR (User Experience Research). UX Research**

Es la disciplina que se centra en investigar, empatizar y comprender el comportamiento de las **personas**, sus **necesidades** y **motivaciones**.

Se basa en técnicas de **observación**, **análisis** y otros métodos para obtener el feedback de los usuarios.

![Elementos der la Experiencia de Usuario de Garret (2002)](img/garret.png)

El **Researcher** es la persona encargada de **empatizar** y encontrar las **necesidades** más allá de lo que las personas dicen.

> Lo que las personas dicen no es lo que las personas hacen, y viceversa.

![Empatizar con](img/empatizar-con.png)

¿Cuánto debe acercarse el diseñador al usuario? Si se acerca demasiado se corre el riesgo de diseñar algo que sólo es válido para un usuario en concreto y se ignoran otros. Si no se acerca lo suficiente el producto no responderá a las necesidades del usuario, sólo es fruto del criterio del diseñador.

## Minimum Viable Product (MVP). Mínimo Producto Viable

Un MVP es un producto con suficientes características para satisfacer a los clientes iniciales. Lo más importante es que nos proporciona retroalimentación para el desarrollo futuro.

**Definición popular y simple**:

> Un MVP es la cosa más pequeña que se puede construir y a su vez ofrece valor al cliente.

Crear un MVP es un proceso iterativo e incremental.

![Crear un MVP](img/mvp.png)

**Así / Así no**

![MVP. Cómo](img/mvp-2.png)

> "Cuando consideres crear tu propio producto mínimo viable, deja que esta simple regla sea suficiente: elimina cualquier característica, proceso o esfuerzo que no contribuya directamente al aprendizaje que buscas".
>
> Eric Ries. Emprendedor americano y autor de *El método Lean Startup*

**¿Qué beneficios nos da un MVP?**

Puede acelerar el aprendizaje del equipo con respecto a lo que el cliente realmente quiere / necesita mientras usa una iteración rápida para lograrlo.

Minimiza la cantidad de horas desperdiciadas del equipo de desarrollo centrándose en una cantidad mínima de funciones para el lanzamiento.

Puede salir al mercado más rápido y, en teoría, comenzar a aumentar los ingresos por ventas más rápido que si desarrollara el producto final con todas las funciones para su lanzamiento.

Se obtiene una ventaja competitiva si otras empresas están contemplando ingresar al mercado en el que se está enfocando.

## Libros recomendables de UX/UI y otros recursos

https://www.espacioux.com/librosux

https://www.espacioux.com/blog. Blog del profesor del Curso.

[Artículo de blog sobre Chat GPT y UX Design - 1](https://tldv.io/blog/chatgpt-prompts-for-ux-design/)

[Artículo de blog sobre Chat GPT y UX Design - 2](https://www.thefountaininstitute.com/blog/chat-gpt-ux-design)

[Artículo de blog sobre Chat GPT y UX Design - 3](https://medium.com/@jeffreyhumble/11-advanced-ux-tasks-you-should-start-automating-in-chat-gpt-63a6a1b45cc1)

[Excelente video sobre cómo funciona Chat GPT, beneficios y más](https://youtu.be/FdZ8LKiJBhQ)

## Investigación de usuarios. User Research

Lo que todas las técnicas de investigación de usuarios tienen en común es que ayudan a colocar a las personas en el centro del proceso de diseño y del producto.

Tres razones para hacer una investigación de usuarios:

1. Para crear diseños que sean **verdaderamente relevantes** para los usuarios.
2. Para crear diseños que sean **fáciles y agradables de usar**. La alta usabilidad para el usuario como foco principal. Que los prductos brinden ua. excelente experiencia de usuario.
3. Para comprender el retorno de la inversión (ROI) del diseño UX. ROI define el nivel de beneficio o no de una acción/inversión.

## Análisis de competidores. Tipos de competidores

Hay dos tipos de competidores: directos e indirectos.

**Competencia directa**

Son todos aquellos que resuelven un problema igual o casi igual al nuestro y que lo hacen para el mismo segmento de usuarios en el que estamos nosotros. Es la más sencilla de encontrar, obviamente.

**Competencia indirecta**

Son aquellos que no resuelven lo mismo que nosotros, pero pueden llegar a satisfacer parcialmente las mismas necesidades.

Por ejemplo:

- Necesidad de hidratarse.

- Necesidad de energizarse.

- Necesidad de algo dulce.

## Mapa de competencia

El mapa de competencia consiste en ubicar a nuestros competidores en un mapa compuesto por dos ejes. Cada eje representa una variable de análisis.

**Objetivo**: monitorear la competencia y detectar áreas de oportunidad.

![Mapa de competencia](img/mapa-competencia.png)

Este es un ejemplo. Las variables de cada eje lque se deaen mapear las definimos nosotros.

![Mapa de competencia](img/mapa-competencia-2.png)

## Estrategia de oceanos azules y rojos

![Oceanos](img/oceanos.png)

En el ejemplo de arriba de las marcas de automóviles, el oceano rojo sería el sector clásico y accesible, y los azules el deportivo y accesible, y el exclusivo y clásico

**Oceanos azules**;

- Ampliar las fronteras del mercado.
- Realizar un mapa estratégico.
- Explorar otros terrenos más allá de la demanda actual.
- Diseñar una estrategia de operación.
- Solucionar obstáculos.

Principios de la estrategia de océanos azules:

1. Ir más allá de la demanda. No pensar solamente en los consumidores existentes.
2. Reconstruir los límites del mercado. Pensar *fuera de la caja*, más allá del negocio.
3. La secuencia estratégica correcta. La implementación tiene que ser factible.

## Innovación y creatividad

Todo está inventado.

Un excelente documental acerca de los elementos de la creatividad y que todos los inventos en el mundo son un remix. 

Está dividido en 4 videos.

[Parte 1](https://youtu.be/fLhIovovPm4)

[Parte 2](https://youtu.be/8hGUfZ48qN0)

[Parte 3](https://youtu.be/bhmbqr4QdZA)

[Parte 4](https://youtu.be/yizH3LvyvCM)

## Benchmarking

Es una técnica basada en conocer mejores prácticas fuera de la empresa (a veces puede originarse dentro de la misma empresa).

**Su objetivo es**: llegar a la mejora continua.

**No es Copy y Paste**.

No es trasladar las soluciones de los otros a nuestros problemas, sino **entender el proceso de esa solución**.

Hay tres tipos de benchmarking:

### Benchmark interno

Se aplica dentro de la empresa.

Por ejemplo, cuando se toman como referencia las prácticas utilizadas por una determinada área de la empresa que, a diferencia de otras, está obteniendo muy buenos resultados.

### Benchmark competitivo

Se aplica con empresas que son competidoras directas.

Por ejemplo, cuando se toma como referencia las prácticas de la competencia en donde esta nos supera.

### Benchmark funcional

Se aplica con empresas que podrían ser o no competidoras directas.

Por ejemplo, cuando se toman como referencia las estrategias de una empresa que es líder en un sector diferente al de la propia.

### Proceso de benchmarking

**Definición de objetivos**

Se refiere a la necesidad de determinar los aspectos que van a ser sometidos a benchmarking, es decir, los aspectos en donde se quiere mejorar, por lo tanto, se van a tomar como referencia de la competencia.

¿Dónde queremos mejorar? ¿En el contenido, en nuestros flujos, en la forma de trabajar como empresa?...

**¿Con quién nos comparamos? El ¿QUIÉN?**

Aquellas empresas, competidoras directas o no, que tengan lo mejor o hagan lo que se quiere mejorar. Por ejemplo, mayores ventas.

**¿Qué información recolectamos?**

Aquella información relacionada con los aspectos que van a ser sometidos a benchmarking de las empresas seleccionadas. Por ejemplo, los medios publicitarios, mensajes publicitarios, público objetivo, promociones de ventas...

**Analizar**

Una vez que se ha recolectado la información se procede a su análisis, a fin de comparar los aspectos de las empresas seleccionadas con los propios y con los de otras empresas.

Además, se intentará identificar los mejores y los que sean posibles de aplicar en la propia empresa.

**Adaptar las mejoras**

Una vez definidos los aspectos a aplicar los adaptamos, mejorándolos y aplicándoles el plus que los haga diferenciales y propios de nuesta empresa.

## Análisis de contexto

El contexto en Experiencia de Usuario **NO** es igual a lo que nos rodea. Es:

- Emocional

- Personal

- Espacial

- Temporal

- Cultural

- Social

- Político

- Etc

> Cuando hablamos de contexto en la Experiencia de Usuario, nos referimos al entorno pero también a todo lo que sucede en la cabeza del usuario.

¿Por qué analizamos el contexto? El contexto influye en el uso del producto y en cómo se va a usar el mismo.

Conocer el contexto del usuario permite **potenciar el producto**.

**Entender el contexto para solucionar y comprender el mundo**

Comprender el contexto es muy importante para comprender a los usuarios en su hábitat, encontrar *insights (hallazgos)* y preconfigurar soluciones a problemas de diseño.

Permite potenciar las habilidades de nuestro diseño para ser más empíricos y eficaces. Ofreciendo experiencias **personalizadas** y **centradas en las personas**.

Hay  un excelente caso de estudio de parte de BBVA y IDEO acerca del contexto.

Se puede comprobar la importancia del contexto y cómo podemos mejorar **la experiencia de ir a un cajero automático**. Una experiencia que para muchos puede ser abrumadora, con una sensación de estrés o nervios.

**Link:** [Enlace al Case Study en Youtube](En este artículo te adjunto un excelente caso de estudio de parte de BBVA y IDEO acerca del contexto.

Verás la importancia del contexto y cómo podemos mejorar **la experiencia de ir al cajero**. Una experiencia que para muchos puede ser abrumadora, con una sensación de estrés o nervios.

[Enlace al Case Study en Youtube](https://youtu.be/x-DLQp9xb20)

**Cómo conocer el contexto**

- Contexto del producto: benchmark, análisis de la competencia.
- Contexto del usuario: mapping, hipótesis, entrevistas, encuestas.

## Hipótesis

Las hipótesis son afirmaciones o supuestos que los diseñadores UX crean para guiar su trabajo y ayudarles a tomar decisiones informadas durante el proceso de diseño. Suposiciones hechas a partir de unso datos, que sirven de base para iniciar una investigación o una argumentación.

Las hipótesis son una herramienta importante para los diseñadores UX, ya que les permiten establecer una base sólida para su trabajo y validar sus ideas de diseño antes de invertir tiempo y recursos en ellas.

Generando hipótesis de investigación nos acercamos a nuestros usuarios con algo que **estudiar** y **aprender**.

Consejos para escribir buenas hipótesis:

- Escribirlas en afirmativo.
- No escribirlas en modo de pregunta o negación
- Relevantes para lo que investigamos
- Claras y concisas. Hay que ser específico en lo que se plantea como hipótesis.

Pasos para escribir una hipótesis correctamente:

1. Identificar el problema o situación que se quiere resolver o mejorar.
2. Investigar sobre el problema para tener una mejor comprensión del mismo.
3. Formular una pregunta específica que se quiera responder mediante la hipótesis.
4. Establecer una hipótesis (una afirmación) a partir de la pregunta que se formuló previamente.
5. Refinar la hipótesis, mejorarla mediante feedback o más información recopilada.
6. Planificar el experimento o investigación, para probar la hipótesis.

![Hipótesis](img/hipotesis-1.png)

![Hipótesis](img/hipotesis-2.png)

![Hipótesis](img/hipotesis-3.png)

![Hipótesis](img/hipotesis-4.png)

No hace falta que la hipótesis tenga un porcentaje o métrica que medir.

 [Ejemplos de hipótesis](https://www.ejemplos.co/20-ejemplos-faciles-de-hipotesis/)

Una vez realizada una investigación preliminar para comprender y conocer en profundidad el tema o cuestión que se plantea **hay que llegar a un total de al menos 10 hipótesis**.

Aunque un diseñador UX nunca deja de hacer hipótesis.

## Data-Driven Design

Consiste en adaptar los métodos de diseño orientándolos a conseguir unos objetivos. Apoyar nuestras decisiones de diseño en los hallazgos/insights provistos por los datos.

El Data-Driven Design (DDD) (Diseño Guiado por Datos) es una metodología de diseño que utiliza datos para guiar el proceso de diseño y tomar decisiones informadas.

El objetivo principal es crear productos y servicios que se ajusten a las necesidades y preferencias del usuario final, **basándose en datos obtenidos a través de diversas fuentes**.

Diseñar en base a datos, no en opiniones o prejuicios:

- Eliminar prejuicios.
- Medir el impacto real.
- Definir estrategias.
- Generar nuevas hipótesis.
- Encontrar nuevos hallazgos/insights.
- Tomar o descartar decisiones con fundamentos.

Hay dos tipos de datos:

- **Cuantitativos**: son datos que se puede medir y verificar, que nos dan información acerca de las cantidades, es decir, información que puede ser medida y escrita con **números**.
- **Cualitativos**: son informaciones que describen las características, cualidades o propiedades de algo, en lugar de medirlo en números. Los datos cualitativos pueden ayudarnos a entender cómo piensan, sienten y experimentan las personas el mundo que les rodea. Son datos que se basan en observaciones y descripciones en lugar de mediciones exactas.

El análisis cuantitativo:

- ¿Qué está pasando?
- ¿Cuándo?
- ¿Cómo?
- ¿A quién?

El análisis cualitativo:

- ¿Por qué?
- ¿Qué piensan las personas?
- ¿Qué sienten las personas?

Hay numerosas fuentes de datos, información y encuestas disponibles. [Google Académico](https://scholar.google.es/) es un gran recurso para encontrar fuentes fiables. Los organismos públicos responsables de la Estadística también pueden ayudar al respecto. 

## Encuestas vs Entrevistas

Las encuestas aportan información cuantitativa, medible números. No confundir con las entrevistas, que proporcionan información cualitativa (el cómo, qué sienten los usuarios...).

Las encuestas y las entrevistas son herramientas importantes para la investigación:

- La encuesta se realiza a una parte de la población estudiada y busca recopilar datos por medio de un cuestionario.
- Una entrevista se realiza a una persona en estudio. Se obtiene respuestas verbales y datos cualitativos.

En las encuestas se realizan **preguntas cerradas**. Con estas preguntas se obtienen los datos cuantitativos medibles de los usuarios. Los datos obtenidos se pueden visualizar fácilmente con representaciones gráficas.

En las entrevistas las **preguntas son abiertas**. Aportan información cualitativa que nos dice cómo piensa el usuario, qué siente sobre una cuestión.

**Preguntas cerradas**:

- ¿Con qué frecuencia usas [producto/servicio]?
  1. Diariamente
  2. Semanalmente
  3. Mensualmente

- ¿Te resulta fácil encontrar lo que buscas en [producto/servicio]?
  1. Sí
  2. No

- ¿Recomendarías [producto/servicio] a un amigo?
  1. Sí
  2. No

**Preguntas abiertas**:

- ¿Podrías contarme sobre tu experiencia usando [producto/servicio]?
- ¿Cuáles son las cosas que más te gustan de [producto/servicio]?
- ¿Puedes explicar cómo utilizas [producto/ servicio] en tu día a día?

## Encuestas

Para construir una encuesta primero hay que definir:

1. Qué información se quiere recolectar y qué hay que preguntar.
2. El usuario al que se le va a hacer la encuesta.

### Partes de una encuesta

- Introducción clara y breve en la que se explica el propósito de la encuesta.
- Preguntas filtro para comprobar si el encuestado reúne las características necesarias para participar en la encuesta.
- Preguntas que nos van a dar la información que buscamos.
- Cierre para agradecer la participación y despedirse del encuestado. Es un buen momento para recabar información (nombre, dirección, correo, móvil...), obviamente de forma voluntaria.

### Consejos

Las preguntas más importantes van al comienzo.

Marcar respuestas obligatorias. (No estoy de acuerdo).

Agregar respuestas tipo *otro/a*, *ninguno/a* como opciones de la serie de respuestas para poder obtener alguna información medible y objetiva.

La pregunta debe ser simple.

Hablar con el mismo vocabulario/registro de los encuestados.

Si hay un concepto que los encuestados puede que no conozcan hay que aclararlo.

### Google Forms

https://docs.google.com/forms

## Entrevistas

Para preparar las entrevistas hay que seguir unos pasos

1. **Objetivo de la entrevista**. Tener claro qué queremos obtener de la entrevista, qué información necesitamos y cómo la vamos a utilizar. Ejemplo: quiero saber los hábitos de consumo de bebidad alcohólicas.
2. **Selección de los usuarios**. A la hora de buscar los usuarios debemos tener en cuenta que deben ser representativos del público objetivo de nuestro proyecto, y que deben tener experiencias y necesidades relevantes para nuestro objetivo de diseño.
3. **Diseñar el guión**. Es fundamental que el guión sea flexible para que te puedas adaptar a las respuestas y comentarios de los entrevistados. Las preguntas deben ser abiertas, aunque se pueden incluir alguna cerrada (¿Consumes alcohol? - Sí. ¿Qué bebidas alcohólicas consumes? - Uy, de todo...). Hay que evitar preguntas que introduzcan sesgos, o que nos saquen de la neutralidad, o que condicionen las respuestas. Evitar las preguntas que no impliquen o sugieran una respuesta determinada.
4. **Planificación y preparación**. Lugar o plataforma donde se va a desarrollar la entrevista (Google Meet, Zoom). Planificar horario y tiempo de duración. Preparar el equipo necesario (grabadora/móvil, cuaderno y bolígrafo). Se necesita el consentimiento de la persona para poder grabarla, grabar sin permiso es moral y éticamente incorrecto y también es un imperativo legal el consentimiento del entrevistado.

No interrumpir y dejar que el entrevistado hable. El porcentaje debería ser un 30% para el entrevistador, para hacer sus preguntas o repreguntar y el 70% sería lo que el entrevistado nos cuenta. Incluso un 20-80.

No juzgar, ni hacer muecas o expresiones.

Debemos ser neutros en todo momento.  

La gente suele inventar o justificar acciones.

La memoria humana es selectiva.

Las personas tienden a querer quedar bien.

### Los 5 ¿Por qué?

Es una técnica para realizar preguntas iterativas, usadas para explorar las relaciones de causa y efectos subyacentes a un problema particular. Gran parte de nuestro trabajo es entender y hallar el problema raíz.

¿Qué objetivo tiene?

El objetivo principal de esta técnica es determinar la causa raíz de un defecto o problema repitiendo la pregunta *¿por qué?*.

Cada respuesta forma la base de la siguiente pregunta. El **5** en el nombre se deriva de la observación empírica en el número de iteraciones típicamente requeridas para resolver el problema.

## Focus Groups

Un grupo pequeño de personas, generalmente entre 5 a 10, se sientan en la mesa y hablan sobre cosas, sobre su opinion acerca de algún producto, su experiencia pasada con ellos, o sus reacciones a nuevos conceptos.

Los Focus Groups son buenos y rápidos para entender un mínimo conocimiento de los sentimientos de los usuarios y las opiniones acerca de las cosas.

Cuentan con un moderador / facilitador / investigador.

El moderador plantea preguntas de un guión al grupo. Sus respuestas son registradas, a veces por el moderador, a veces por un observador u observadores, y luego analizadas e informadas al final del proceso.

Para poner en marcha un Focus Group:

- Reclutamiento y elección de los participantes. Seleccionar los perfiles pertinentes en función de lo que se se trata.
- Creación del guión del moderador. Definir el propósito del Focus Group y partir de ello redactar las preguntas de las que se obtendrán las respuestas del objetivo inicial. Hay que evitar las preguntas capciosas, que presupongan cosas. Hay que plantear preguntas muy generales, neutras, que no condicionen las respuestas.

------

## Fase Definir

Definir, porque de toda la información que tenemos hay que sintetizar y llegar a un punto concreto.

Definir para:

- Encontrar hacia donde está nuestra problemática.
- Sintetitzar la información de la investigación
- Descubrir nuevas perspectivas
- Tomar decisiones y trabajarlas

¿Qué vamos a definir?:

- Quiénes son los usuarios de nuestra problemática.
- Qué límites tenemos en la problemática.
- El contexto en donde se produce la problemática.
- El negocio que da como resultado la solución.

## UX Personas. User Persona

Las User persona son representaciones ficticias de los usuarios finales de un producto o servicio, basadas en datos reales y observaciones de comportamiento.

Además:

- Es una herramienta para tomar decisiones.
- Es un arquetipo de un humano real.

**¿Para qué sirven?**

Estas representaciones ayudan a los equipos de diseño y desarrollo a comprender mejor las necesidades, objetivos, comportamientos y preferencias de los usuarios, lo que a su vez les permite crear soluciones que satisfagan las necesidades de los usuarios de manera efectiva.

**¿Cuáles son sus objetivos?**

- Representar a un usuario real.
- Descubrir y describir personas exactas.
- Empatizar mejor.
- Poner tanto datos cuantitativos como cualitativos. Saber no sólo lo que ocurre, sino que también el porqué.

User Persona (una persona, su comportamiento) **NO** es Target (Marketing, perfil de un consumidor).

Arquetipo (definido por investigación y análisis) **NO** es Estereotipo (definido por prejuicios y preconceptos).

**¿Qué beneficios tienen?**

Las User Persona son una herramienta valiosa para los equipos de diseño y desarrollo porque les permiten mantener al usuario en el centro del proceso de diseño y tomar decisiones basadas en las necesidades reales de los usuarios.

También:

- Evita que uno proyecte sus propios objetivos, motivaciones, habilidades, etc.
- Evita centrarse en atender situaciones ideales y concretas que no representen a la mayoría de los usuarios.

**¿Cómo se crean?**

A través de una investigación de usuarios, por ejemplo, entrevistas, encuestas, revisión de fuentes, análisis de datos, etc.

**Siempre se debe tener en cuenta a la User Persona**.

### Construir la User Persona

1. Investigación. Todas las técnicas de investigación llevadas a cabo en la Fade de Empatizar
2. Recopilación y análisis de los datos. Analizar los datos cuantitativos y cualitativos y buscar patrones, datos en común que tienen las personas que hemos investigado.
3. Construir la User Persona (Patrones y síntesis).  A partir de los datos recopilados y analizados, se pueden crear perfiles de usuario detallados que representen a los diferentes tipos de usuarios. Estas User Persona deben describir información como la edad, el género, la educación, las metas y objetivos, el comportamiento y las preferencias de los usuarios.
4. Definición de User Persona. Nombre, edad, profesión y ocupación. Sus objetivos, intereses, deseos, metas, necesidades, hábitos, motivaciones, frustraciones, expectativas...

**¿Con qué información creamos a nuestra Persona?**

Analizamos la información recopilada en nuestra investigación de desktop, los datos cuantitativos (encuestas) y los datos cualitativos (entrevistas).

Observamos y analizamos la información para encontrar **patrones**.

**NO se inventa**

Para que nuestra Persona sea una representación literal de un grupo de personas (arquetipo), no debemos inventar su información, deben **ser patrones hallados en nuestra investigación**.

Lo único que debemos inventar (siendo coherentes con la información recabada) es su nombre, edad, profesión y biografía.

## Point ov View (POV). Puntos de Vista

Es el marco que define el problema que vamos a abordar, construido en base a la información y datos durante la fase de empatizar.

![Punto de Vista (POV)](img/pov.png)

![Punto de Vista (POV)](img/pov-2.png)

Usuarios: especificaciones del usuario.

Necesidades: qué problema a abordar.

Hallazgos: insights / hallazgos.

Ejemplos:

Un tenista experimentado necesita autoevaluarse y aprender cómo medir sus entrenamientos, porque quiere convertirse en un tenista reconocido y modelo a seguir

Una joven que vive sola necesita sentir que sabe donde ir a hacer las compras, porque quiere poder probarle a su familia, amigos y su pareja que es realmente una adulta independiente.

**NO se inventa**.

Hay que hacerlo en base a nuestro Usuario Persona, con necesidades e insights que fuimos hallando con el análisis cuantitativo y cualitativo.

**Construimos Puntos de Vista**.

Al menos 3 puntos de vista:

- 1 o 2 usuarios distintos.
- 2 o 3 necesidades distintas.
- 2 o 3 hallazgos distintos.

## Brief

Es una definición, a veces incompleta y no muy clara. Es una breve explicación que vamos a aplicar a nuestra problemática. Ayuda a orientar la exploración en una dirección.

Ejemplo:

"*Queremos mejorar la logística de nuestra eCommerce"*.

Es una descripción demasiado corta, pero aún así puede darnos una dirección hacia dónde explorar.

Debemos tomar ese Brief y mejorarlo para el equipo de diseñadores o el diseñador. Siempre hay que tener en la cabeza el Brief.

Nos permite tener un punto en común, pensar ideas y tomar decisiones en base a él.

Ejemplos de Briefs incompletos y no adecuados:

- "Necesito que mi app sea más moderna".
- "Quiero vender el doble".
- "Hay que cambiar y mostrar algo excelente".
- "Tienen una web y quieren hacerla mejor".
- "Quiero hacer una web como la de Apple o X".

Sin el problema definido afloran ideas y descripciones irrelevantes. **Debemos definir el problema**.

**Hacemos un Brief**.

Definimos la **Situación**, la **Motivación**, la **Mejora** y los **Límites**.

![Brief](img/brief.png)

![Brief. Ejemplo](img/brief-2.png)

Así quedaría el Brief de este ejemplo:

"Cientos de autos ingresan a la ciudad debido a la centralización de las empresas. La gente está siendo impuntual, pierden tiempo y no están utilizando el medio de transporte público. Queremos que baje un 30% el ingreso de autos a la ciudad y aumente un 20% el transporte público. Necesitamos una solución en 6 meses y contamos con un presupuesto de 2000 dólares, también, tenemos límites legales".

**Hay cientos de modelos de Briefing distintos**.

Cada uno tiene sus parámetros, ventajas y desventajas. Tenemos que encontrar el que nos facilite la definición y nos dé más valor.

![Ejemplo de Brief final](img/brief-3.png)

**El Brief es de lo más difícil que vamos a definir.** Puede llevar incluso meses comprender cuál es el **problema real**.

## Critical User Journey Map

En Google, la mayoría de los productos se comienzan usando de base los Critical User Journeys (CUJs). CUJs son una combinación de un objetivo critico y el recorrido de tareas que un usuario debe atravesar para lograr ese objetivo.

**Critical User Journey Map**

Para definirlo utilizamos el siguiente template:

Como **usuario**, quiero un **objetivo**, así que hago una **tarea**.

*As a user, I want a goal so I task*.

Ejemplo:

Como **dueño de una empresa pequeña**, quiero **conectar con mis clientes**, así que **respondo a las reviews**.

*As a small business owner, I want to connect with my customer so l respond to reviews*.

Ejemplo de Actividad:

Como **comediante en Instagram**, quiero **lograr ser popular y reconocido**, así que **mejoro mi contenido analizando el feedback de los usuarios**.

*As a comedian on Instagram I want to achieve popularity and recognition so I improve my content by analyzing user feedback*.

------

## Fase Idear

La fade de Idear es para explorar, definir soluciones a nuestra problemática planteada en el Brief y ver qué podemos hacer.

Para tener buenas ideas debemos tener bien definido el problema que queremos resolver.

Hay técnicas para potenciar la creatividad.

### Brainstorming

### How Might We

1. **How o Cómo**: supone que hay soluciones para esa pregunta, proporcionando la confianza creativa.
2. **Might o Podríamos**: indica que podemos poner ideas por ahí que podrían funcionar o no, pero que de todos modos, están bien.
3. **We o Nosotros**: como su significado indica, sugiere que vamos a hacerlo juntos y construyendo sobre las ideas de los demás.

Ejemplo:

Punto de Vista (POV):  Las adolescentes necesitan comer alimentos nutritivos para prosperar y crecer de manera saludable

How Might We?:

- ¿Cómo podemos hacer que la alimentación saludable sea atractiva para las mujeres jóvenes?
- ¿Cómo podemos inspirar a las adolescentes hacia opciones de alimentación más saludables?
- ¿Cómo podemos hacer de la alimentación saludable algo a lo que aspiran las adolescentes?
- ¿Cómo podemos hacer que los alimentos nutritivos sean más asequibles, más baratos?

El grupo de preguntas desemboca en un **Brainstorm** grupal para generar posibles soluciones.

### Proceso para idear

![Proceso para idear](img/proceso-idear.png)

#### Fase Escenarios

Hacemos escenarios para no perder el foco del objetivo y nuestra Persona. También para idear luego en un marco delimitado.

Tenemos que pensar que es lo que vamos a ofrecerle a nuestro usuario. No hay que pensar características ni funcionalidades.

![How Might We](img/howmightwe.png)

![How Might We](img/howmightwe-2.png)

Ejemplos:

![How Might We. Ejemplos](img/hmw-ejemplos.png)

¿Cómo podríamos **motivar** a **Pedro** para que pueda **concurrir a conciertos más frecuentemente**?

¿Cómo podríamos **ayudar** a **un restaurante** para que pueda t**ener un impacto positivo en las personas veganas**?

¿Cómo podríamos **transmitir confianza** a **clientes inseguros** para que puedan c**onfiar en la transparencia de nuestro servicio**?

#### Fase Generar ideas

Hacer las preguntas correctas e innovar, con un fuerte enfoque en la UX Persona, el Brief y los Puntos de Vista (POV)

Ir más allá de las soluciones obvias y aumentar el potencial de innovación de su solución.

Si se trabaja con un equipo hay que reunir las perspectivas y fortalezas de los miembros del mismo.

Volver al área de innovación (**oceano azul**).

No hay ideas buenas o malas. Hay ideas relevantes o irrelevantes para nuestros escenarios.

Escribir tantas ideas como sea posible. Describirlas lo mejor posible, con detalles y contexto. Breves, no más de 20 palabras

En los Escenario se plantean las preguntas. En la generación de ideas se aportan posibles respuestas a esas preguntas.

![Ideas](img/ideas.png)

#### Fase Evaluar las Ideas

##### Análisis FODA/DAFO

El análisis FODA son siglas que representan el estudio de las **Fortalezas**, **Oportunidades**, **Debilidades** y **Amenazas** de una empresa, un mercado, persona o hasta una idea.

Esta herramienta es aplicada a cualquier situación en la que se necesite un análisis o estudio.

Es una herramienta de planificación estratégica, diseñada para realizar un **análisis interno** (Fortalezas y Debilidades) y un **análisis externo** (Oportunidades y Amenazas).

Nos va a ayudar a descubrir oportunidades y a entender las debilidades de nuestras ideas, eliminando también las amenazas que podrían tener.

![FODA](img/foda.png)

Las fortalezas y debilidades son internas a la solución. Ejemplo: la empresa, las personas de la empresa

Las oportunidades y amenazas están relacionadas a los factores externos. Ejemplo: el contexto, la competencia, los límites.

Fortalezas:

- Ventajas de la idea
- ¿Qué valor, beneficio aporta la idea a la UX Persona?

Visto desde una **perspectiva interna** y el **punto de vista** de las personas.

Debilidades:

- ¿En qué afecta la idea a las UX Personas?
- ¿Qué podría mejorar de la idea? ¿Tiene una debilidad? ¿Se puede corregir?

Visto desde una **perspectiva interna** y el **punto de vista** de las personas.

Oportunidades:

- ¿Qué factores externos favorecen la idea?
- ¿Qué se podría mejorar de la idea?

Pensando en las tendencias, la realidad externa, la competencia, el mercado, políticas gubernamentales, etc.

Amenazas:

- ¿Qué factores externos amenazan la idea?
- ¿Qué competidores o ideas de competidores existen?

Pensando en las tendencias, la realidad externa, la competencia, el mercado, políticas gubernamentales, etc.

No importa la cantidad de debilidades, amenazas, oportunidades o fortalezas, sino el peso de ella.

Ejemplo:

Hacer ejercicio en el parque

![Ejemplo FODA](img/foda-ejemplo.png)

El peso de la amenaza de la lluvia es muy determinante, tiene mucho peso. Los factores externos no se pueden modificar. Las debilidades internas sí se pueden mejorar.

#### Fase Determinar la Viabilidad

![Viabilidad](img/viable.png)

![Matriz Personas/Negocio](img/matriz-personas.png)

Las ideas que están en el cuadrante 1 son las que aportan más valor a las personas y son las más rentables y factibles.

![Matriz Personas/Tecnología](img/matriz-personas-2.png)

En las matrices el orden de los cuadrantes marca el orden de realización: 1 prioritario, 2 y 3 no se van a descartar pero se acometerán después, y 4 se descarta.

No somos expertos en todo.

No somos expertos en negocios, diseño UX y en la programación de sistemas. Vamos a pensar lo que creemos cierto, investigamos y/o preguntaremos a quien sepa al respecto.

Estas matrices se van a implementar en base a nuestros conocimientos, lo que se piensa en el equipo de trabajo,  y a lo que podamos averiguar preguntando a expertos, buscando en Internet...

## Journey Mapping

Es una herramienta que se puede utilizar en una etapa de investigación y también de ideación.

Es una herramienta que permite plasmar visualmente todo lo observado, analizado y aprendido durante nuestro proceso de investigación.

Es una herramienta basada en la investigación que los equipos de diseño utilizaron para dar relieve a las experiencias típicas del cliente y los factores involucrados. Permiten a las empresas aprender más sobre los usuarios objetivo.

Journey Map resulta muy efectivo en el  momento de identificar los diferentes puntos de contacto con nuestro producto o servicio y nos ayuda a ponernos en el lugar y en cómo piensa un tipo de usuario.

La recomendación para hacer una Journey Map es KISS: Keep It Simple, Stupid.

El principio **KISS** establece que la mayoría de sistemas funcionan mejor si se mantienen **simples** que si se hacen complejos. La **simplicidad** debe ser mantenida como un objeto clave.

Vamos a dividir una experiencia en distintos momentos.

> "Los datos comúnmente no logran comunicar las frustraciones y experiencias de los clientes. Una historia puede hacer eso, y una de las mejores herramientas de narración en los negocios es el Journey Map ".
>
> Paul Boag, diseñador de UX, consultor de diseño de servicios y experto en transformación digital

El Journey representa la historia desde el punto de vista del usuario, no del producto. Debemos identificar **situaciones reales por las que atraviesa la Persona**, indistintamente de lo que haga el producto.

### Momentos

Momento por el que atraviesa nuestra Persona.

Ejemplo de la detección de momentos en un Journey Map:

Momento 1: Carla decide consultar con un psicólogo.

Momento 2: conoce nuestro servicio.

Momento 3: comienza a utilizar el servicio por primera vez.

Momento 4: busca psicólogos.

Momento 5: contacta/reserva turno con el psicólogo.

Momento 6: se encuentra con el psicólogo.

Momento 1: post-consulta.

### Acciones

Las acciones de la Persona que realiza ese Momento,

Ejemplo de Acciones de un Momento.

Momento 1: Carla decide consultar con un psicólogo

Acciones 1:

- Habla con un amigo.
- Googlea información sobre psicólogos.
- Ve en Instagram publicaciones de psicología.
- Pide recomendaciones en un grupo.
- Lee un libro de un psicólogo reconocido.

### Sentimientos y emociones

Emociones que generan las Acciones de la Persona.

### Pain Points

Puntos de dolor que la Persona tiene en esa Acción y Momento.

Ejemplo: Carla no encuentra un psicólogo que le genere confianza.

Preguntas a hacerse para encontrar más Pain Points

¿Y qué tal si Carla no puede / no sabe / no quiere / no confía en...?

¿Y qué tal si el contexto no ayuda / no funciona Internet / no recibe respuestas de sus amigos / no encuentra lo que busca...?

¿Y qué tal si la experiencia no alcanzó sus expectativas? ¿Y si se confunde de fecha o lugar? ¿Y si se arrepiente?

### Oportunidades

Oportunidades de mejora para esos Paint Points.

Ejemplo:

Paint Point: Carla no confía que el psicólogo tenga experiencia.

Oportunidad: agregar reseñas de clientes anteriores.

![Journey Map](img/journey-map.png)

![Journey Map, Ejemplo](img/journey-map-ejemplo.png)

## Apps vs Webs

### Ventajas de la Web

- Son económicas para desarrollarlas.
- Se pueden crear fácilmente sin necesidad de ningún tipo de programación especial.
- Su base esta construida mayormente con HTML, CSS.
- Son sencillas, rápidas y cómodas.
- Se actualizan fácilmente.
- Usan bases de datos para gestionar el contenido.
- Hay infinidad de plantillas, templates y contenido gratuito.
- Hay generadores de sitios web.

### Desventajas de la Web

- En dispositivos como tablets o celulares no sacan provecho al hardware.
- No utilizan los features/características de un teléfono movil como: captura de imagen y vídeo, entrada desde un micrófono, sensor de proximidad, Face ID, barómetro, entre tantas más.
- Requieren de una conexión a internet para funcionar.

### Ventajas de la App

- Le sacan todo el provecho al hardware, ya sea en tablets o celulares.
- Tienen una mejor experiencia de usuario, adaptándose a todas las características del celular, y una mejor performance.
- Pueden disponer de un acceso completo al dispositivo, en software y hardware.
- Tienen integración. Con wereables (smartwatchs).
- No requieren de conexión a internet para funcionar. (No estoy de acuerdo).
- Visualización y promoción de las mismas en las tiendas de apps.

### Desventajas de la App

- Costo y tiempo de desarrollo alto (hasta 5 veces más que una web).
- Requieren de diferentes lenguajes de programación y habilidades según el sistema operativo (sea Swift por ejemplo para iOS y Java para Android).
- Requieren de actualizaciones de parte del usuario.
- Hay menos desarrolladores que de Web.
- Requieren de un mantenimiento mayor.

Qué elegir:

- ¿Qué prefiere tu Persona?
- ¿Qué sistema móvil utiliza?
- ¿Qué están haciendo tus competidores? ¿Eligieron una aplicación móvil o una web?
- ¿Cuál es tu presupuesto?

**Debemos analizar el contexto**

¿Cuándo es más probable que utilicen nuestro producto?

- Al aire libre. Apps
- Coche y transporte público. Apps
- Avión. Apps
- Oficina. Webs y apps
- Casa. Webs y apps

**A preguntarse**

¿Necesitas acceso a las funciones del teléfono?

¿Se beneficiarán los usuarios de la información proporcionada en función de su ubicación actual (GPS)?

¿Vas a aprovechar las funciones de cámara, giroscopio o sensores?

¿Nuestro tipo de persona usará el producto sin conexión?

## iOS vs Android

![iOS vs Android](img/ios-vs-android.png)

### iOS

- Las aplicaciones de iOS recaudan hasta un 30% más que las de Android.
- Más *exclusivo*, ya que para desarrollar aplicaciones para iOS se requiere un Mac (valen a partir de 1000 dólares).
- Menor cantidad de desarrolladores de iOS que Android.
- Los usuarios de iOS están más predispuestos a gastar dinero en aplicaciones.
- Hay pocos usuarios de iOS en el mundo comparado con Android.
- Sistema de código cerrado (proporciona menor libertad a los desarrolladores).
- Costos para  publicar aplicaciones: 100 dólares al año.

### Android

- Mayor cantidad de desarrolladores en el mundo.
- Mayor cantidad de usuarios disponen de un celular Android.
- Sistema de código abierto (más libertad para el desarrollador).
- Es más sencillo dar de alta una aplicación en Android porque la Play Store no limita con protocolos tan estrictos como puede ser la App Store de iOS.
- Android domina el mercado y tiene un alcance mucho mayor.
- Android es el sistema por defecto en la mayor cantidad de dispositivos del mercado.
- Costos para  publicar aplicaciones: 25 dólares (licencia sin vencimiento).

## Capacidades de un celular

**Sensor de ubicación:**

Coordenadas de ubicación precisas desde GPS

**Giroscopio y acelerómetro:**

Orientación del dispositivo y sensor de movimiento

**Sensor de difusión de audio:**

Transmisor FM

**3D Touch:**

Diferenciar la presión que ejerce el dedo sobre la pantalla

**Sensor biométrico:**

Detector de huellas dactilares

**Face ID:**

Tecnología de reconocimiento facial

**Sensor del dispositivo:**

Bluetooth

**Sensores de imagen y video:**

Captura de imagen y video

**Sensor de proximidad:**

Proximidad del dispositivo a otros objetos y personas

**Barómetro:**

Medición de presión atmosférica y de altitud.

**Sensores táctiles:**

Entrada multitáctil de uno o más gestos

**Sensor de luz/oscuridad:**

Detección de luz ambiental

**Magnetómetro:**

Rumbo direccional mediante una brújula digital

## Narración

**¿Qué vamos a narrar?**

Nuestra oferta de valor.

La narración de nuestra oferta de valor es clave, porque puede determinar el éxito o el fracaso de una propuesta, sin importar la calidad de la misma.

Ejemplos de narración:

![Narración. Ejemplos](img/narracion-ejemplos.png)

Método bullets: 

Ejemplo: Netflix

![Método Bullets](img/metodo-bullets.png)

![Método Bullets](img/bullets-2.png)

> Lo importante y lo que tenemos que narrar es el **qué**, no el **cómo** va a ser.

--------

## Fase de Prototipar

Esta fase se pueden dividir en distintos roles:

- Arquitecto de la información.
- Diseñador de interacción.
- Diseñador UI.

### Arquitecto de la información

Es el encargado de crear la estructura en un sitio web o aplicación.

Su tarea principal es analizar la arquitectura digital ya existente del sitio o app e implementar mejoras, o crearla desde cero.

**La arquitectura de la información es la disciplina con mayor impacto en la usabilidad de una interfaz**.

Si algo sale mal o bien, recae mayormente en la arquitectura de la información.

El arquitecto de la información debe ponerse en el lugar del usuario para establecer qué buscará cuando llegue al sistema, sitio o aplicación.

Para ello se realizan varios estudios entre ellos de las necesidades, los comportamientos y las motivaciones del usuario.

Algunas de las funciones serán:

- Agrupar contenidos que estén relacionados y cómo se van a llamar dichos grupos.
- Hacer la jerarquía de qué contenidos tienen mayor importancia para el usuario.

### Diseñador de interacción

Algunas de las tareas que va a hacer el diseñador de interacción es definir cómo se va a interactuar con el sistema, Web o Aplicación.

De ser una aplicación, por ejemplo, va a definir cómo van a ser los gestos y qué acciones de los dedos provocan qué resultados.

### Diseñador UI

Es el encargado de crear visualmente la interfaz del producto, para que vaya acorde a la experiencia de usuario y el branding de la empresa.

### Arquitectura de la información

#### Modelos mentales

Los modelos mentales son todo aquello que nuestra mente guarda que cree que usará o necesitará recordar más adelante.

Pero lo más importante es que son representaciones y percepciones que tenemos de una realidad externa.

Pueden variar mucho entre distintas personas y más aún entre personas de distintas culturas, creencias y edades.

Se basan en lo que la persona cree que es real.

Constantemente van variando y son creadas según las experiencias previas de la persona.

La Arquitectura de la información tiene como fin buscar lo objetivo y racional. El modelo mental *justifica* las decisiones y acciones de la persona.

Ejemplo:

Lo que los usuarios crean saber acerca de una interfaz impacta mucho en cómo la van a usar.

A la hora de diseñar un producto o servicio digital debemos contemplar el mayor numero de modelos mentales.

¿Para qué reinventar la rueda?

¿Para qué hacerle aprender al usuario algo nuevo?

#### Card Sorting

Hay que hallar un patrón en los modelos mentales de los usuarios. **Ningún modelo es mejor o peor, sólo distintos**.

![Card Sorting](img/card-sorting.jpg)

Hay tres tipos de card sorting:

**Card sorting abierto**

Es el más común, donde los usuarios son libres de asignar nombres a los grupos que ha creado y a organizar sus tarjetas. Tiene como fin descubrir y definir el tipo de clasificación de categoría más correcto de utilizar.

**Card sorting cerrado**

Este tipo de card sorting es más restringido. Se le pide a los participantes que clasifiquen contenido en categorías que ya se fijaron previamente. Es el más rápido de aplicar dado que existe un camino ya delimitado por los moderadores.

**Card sorting mixto**

Es una mezcla de ambos. Lo que se hace habitualmente es iniciar el proceso con una tarjeta abierta. Con ella, los usuarios deben ordenar los contenidos que el moderador definió. También está la opción de que los participantes creen otras categorias que tengan sentido para ellos.

El card sorting se recomienda realizarlo de forma presencial, pero también se puede llevar acabo online y de forma remota.

Hay muchas [herramientas online](https://www.card-sorting.com/card-sorting-tools/), de pago no, para realizar este estudio. Figma tiene un template para realizarlo. [OptimalSort](https://www.optimalworkshop.com/) es la opción más profesional, es de pago.

#### Tecnología

Los diseñadores UX tienen el trabajo y la responsabilidad reducir la creciente brecha entre el acelerado avance tecnológico y las personas.

¿Cómo?

Haciendo interfaces más humanas.

Los diseñadores UX no diseñan interacciones. Diseñan relaciones.

#### Interfaz de usuario

Es el espacio donde se articula la interacción entre el cuerpo humano, la herramienta o artefacto y el objeto de la acción.

![Interfaz](img/interfaz.png)

> "La mejor interfaz es la no-interfaz"
>
> Golden Krishna

La experiencia es mejor cuantos más sentidos (tacto, oído, vista, gusto, olfato) intervengan.

#### Guionizar el flujo del usuario

A la hora de diseñar una app o web debemos separar el objetivo que persigue nuestro diseño del aspecto que tenga como también de la sensación que nuestro diseño provocará en el usuario.

Por ello, **es vital diseñar teniendo en mente siempre lo que el usuario necesita hacer en esa app o web para integrarlo en el diseño de la formas más eficiente posible**.

#### User Flow. Flujo de usuario

Es el trayecto o proceso que siguen diferentes usuarios para realizar todas las tareas posibles que permita el sistema.

Al haber muchos tipos distintos de usuarios y personas, con distintos modelos mentales, conocimientos y distintas experiencias vividas debemos enfocar el User Flow para que impacte a la mayor cantidad de usuarios posibles.

**Elementos del User Flow**

![User Flow. Elementos](img/user-flow.png)

![User Flow](img/user-flow-2.png)

Flecha que indica para donde continua el flujo: **➝**

Flecha que conecta una acción a otras sin que continúe: —————

Ejemplo:

![User Flow. Ejemplo](img/user-flow-ejemplo.png)

¿Cómo se hace?:

- Se revisa el diálogo que se hizo persona a persona.
- Pensamos y lo adaptamos para la relación persona-máquina.

### Patrones de diseño

Son soluciones ya creadas y probadas para problemas comunes: "¿cómo hago un menú?", "¿dónde va este icono?", "¿qué tamaño de tipografía uso?"...

Son las grandes empresas las que se encargan de modificar o crear patrones de diseño.

#### Patrones de navegación

Navegación jerárquica

![Navegación jerárquica](img/navegacion-jerarquica.png)

Navegación horizontal

![Navegación horizontal](img/navegacion-horizontal.png)

Patrones en aplicaciones móviles:

Tap Bar Android

![Tap Bar Android](img/tap-bar.png)

Tap Bar iOS

![Tap Bar iOS](img/tap-bar-2.png)

Menú hamburguesa (Navegation Drawer)

Patentado por Android. 

Material Design recomienda: 

- Cinco o más destinos de nivel superior. 
- Aplicaciones con dos o más niveles de jerarquía de navegación.
-  Navegación rápida entre destinos no relacionados.

**Patrones Android e iOS**

**Android (Google Material Design)**

- [**Buttons**](https://material.io/components/buttons) (Botones)
- [**Cards**](https://material.io/components/cards) (Tarjetas)
- [**Dialogs**](https://material.io/components/dialogs) (Cuadros de diálogo)
- [**FAV**](https://material.io/components/buttons-floating-action-button) (Botón de acción flotante)
- [**Lists**](https://material.io/components/lists) (Listas)
- [**Menus**](https://material.io/components/menus) (Menúes)
- [**Snackbars**](https://material.io/components/snackbars) (Barras de notificación)
- [**Text Fields**](https://material.io/components/text-fields) (Campos de texto)

**iOS (Apple HIG)**

- [**Action Sheets**](https://developer.apple.com/design/human-interface-guidelines/ios/views/action-sheets/) (Cuadros de acción)
- [**Alerts**](https://developer.apple.com/design/human-interface-guidelines/ios/views/alerts/) (Alertas)
- [**Buttons**](https://developer.apple.com/design/human-interface-guidelines/ios/controls/buttons/) (Botones)
- [**Context Menus**](https://developer.apple.com/design/human-interface-guidelines/ios/controls/context-menus/) (Menúes de contexto)
- [**Sliders**](https://developer.apple.com/design/human-interface-guidelines/ios/controls/sliders/) y [**Steppers**](https://developer.apple.com/design/human-interface-guidelines/ios/controls/steppers/)
- [**Text Fields**](https://developer.apple.com/design/human-interface-guidelines/ios/controls/text-fields/) (Campos de texto)

### Patrones Web

#### Estados de botones

![Estados de los botones](img/botones.png)

#### Hero

![Hero](img/hero.png)

![Hero](img/hero-2.png)

![Hero](img/hero-3.png)

![Hero](img/hero-4.png)

#### Material de imágenes e ilustraciones

**Ilustraciones**

https://undraw.co/illustrations

https://www.vecteezy.com/

**Imágenes**

https://www.pexels.com/es-es/  <- *¡También tiene videos!*

https://unsplash.com/

https://burst.shopify.com/

#### Onboarding

![Onboarding](img/onboarding.png)

![Onboarding](img/onboarding-2.png)

![Onboarding](img/onboarding-3.png)

Ejemplos de pantallas de onboarding.

https://www.useronboard.com/user-onboarding-teardowns/

https://1stwebdesigner.com/mobile-app-user-onboarding/

#### Blank States

Los blank states (estados vacíos) son momentos en al experiencia de un usuario con un producto en los qye no hay nada que mostrar.

¿Cuándo se utilizan?

- Cuando el sitio o app aún están vacías de contenido cargado por el usuario.
- Cuando queremos motivar al usuario a comenzar o a agregar algo.
- Cuando queremos mencionarle al usuario qué va a haber en esa pantalla cuando comience a utilizar el producto.
- Cuando necesitamos comunicar un error.
- Cuando no hay datos ni información que mostrar.

Las pantallas en blanco sin orientación pueden generar confusión, incertidumbre y decepción. Esto puede resultar en mayores tasas de abandono y menor satisfacción general con el producto.

>"Good design is good business"
>
>John Maeda



![Blank States](img/blank-states.png)

![Blank States](img/blank-states-2.png)

Ejemplos de Empty States

https://emptystat.es/

#### Sign Up y Sign In

- Pedir la menor cantidad de datos posible.
- Dar más de una opción para registrarse.
- Verificar que los textos y las visuales tengan consistencia respecto a las demás pantallas.

![Sign Up/Sign In](img/registro.png)

![Registro](img/registro-2.png)

### Usabilidad y jerarquía visual

![Jerarquía visual](img/jerarquia-visual.png)

### Navegación

En la web no hay noción de la escala, de la dirección, ni de la posición.

El propósito de la navegación es que nos diga dónde estamos y cómo movernos adecuadamente en el sitio.

### Breadcrumbs (migas de pan)

Las migas de pan te muestran tu recorrido desde el inicio hasta donde estás en el sitio. Es también una forma fácil de moverse hacia atrás, hacia niveles superiores en la jerarquía del sitio.

Utilizar este recurso cuando hay al menos tres niveles jerárquicos.

- Colocarlo siempre arriba.
- Utilizar `>` entre los niveles.
- Resaltar en negrita o de otro modo el último ítem.

### CTAs. Copys. Textos de los botones 

Un CTA (Call to Action) es un botón o enlace que busca atraer clientes potenciales y convertirlos en clientes finales.

Buenas prácticas para hacer CTAs correctos:

- Focalizar el tipo de usuario al que apuntamos.
- Crear un buen texto (copy). Corto.
- Una buena ubicación.
- Consistencia en el diseño.

Los textos de los CTAs no deben de ser ambiguos. Si un usuario no tiene idea de a dónde lo va a llevar el botón, posiblemente no lo toque. El ser específico es la clave.

![CTA](img/cta.png)

### Recurso para Prototipos y Flujos de apps

[UX Archive](https://uxarchive.com/)

## UX Writing

Es la práctica que diseña con palabras la interacción entre una persona, un usuario, y un producto digital como puede ser una web, aplicación etc.

El rol de UX Writer se encarga de escribir todo lo que hace a la interfaz de un producto o servicio digital, toda esa comunicación y textos que el usuario va a leer o escuchar para lograr el objetivo planteado.

Diseñar es comunicar. Los UX Writers diseñan (comunican) desde el lenguaje escrito.

![UX Writing](img/ux-writing.png)

Un UX Writer debe ser:

- Estratégico.
- Empático.
- Creativo.
- Narrador.
- Concreto.
- Claro.
- Sensible.
- Inclusivo.

En marzo de 2013 se contrata al primer UX Writer. Fue en YouTube.

Buenos ejemplos de UX Writing:

- [Artículos de Despegar sobre UX Writing](https://medium.com/ux-despegar-com/tagged/ux-writing)

¿Por qué hay que usar contenido real en lugar de Lorem Ipsum?

- Para hacer el diseño en base al contenido.
- Usar el contenido real ahorra tiempo y dinero.
- Ayuda a que los usuarios entiendan el contexto y el contenido.
- Genera una mejor impresión para usuarios y clientes.

Escribir el contenido es parte del proceso de diseño. Diseño y contenido van de la mano.

El diseño basado en contenido no sólo mejora el proceso de diseño, también tiene múltiples efectos positivos para el negocio:

- Aumenta las posibilidades de éxito al transmitir el mensaje deseado.
- El diseño está hecho para apoyar y mejorar el contenido (y no al revés).
- Más compromiso con el contenido. Resultado: la retención de clientes es mayor.

> "El contenido precede al diseño. El diseño en ausencia de contenido no es diseño, es decoración. Y la decoración no transmite tu mensaje. El contenido lo hace."
>
> Jeffrey Zeldman

![Contenido](img/contenido.png)

¿Dónde actúa el UX Writer?

- Flujos de acción.
- Páginas de llegada.
- Botones. CTAs.
- Preguntas frecuentes.
- Mails de producto.
- Términos y condiciones.
- Modales.
- Formularios.
- Enlaces o links.
- Páginas de explicación de los productos o servicios.
- Respuestas que da el sistema al usuario.
- Chatbots.
- Contenidos para interfaces de voz.
- Contenidos específicos para accesibilidad.
- Notificaciones.
- Etiquetas `alt` en las imágenes.

Hay una excelente herramienta para descifrar cómo comunicarnos según el país al que vayamos a escribir contenido. Es una herramienta fundamental y nos ahorra tener que estar googleando o preguntando a personas de dicha región. 

 [Google Trends](https://trends.google.es/trends/)

¿Qué es comunicar?

La comunicación es un proceso que consiste en la transmisión e intercambio de mensajes entre un emisor y un receptor.

Además del emisor y receptor participan elementos como:

- Código (lenguaje).
- Canal de comunicación (el medio).
- Contexto (dónde, cuándo, cómo, etc).
- Ruido (problemas en la comunicación).
- Feedback o retroalimentación (respuesta).

**Modelo de Jakobson aplicado a web/app**

![Modelo de Jakobson](img/modelo-jakobson.png)

Funciones básicas de la comunicación:

- Informativa.
- Persuasiva.
- Formativa.
- De entretenimiento.

## Prototipado

Un **prototipo** es un modelo (representación, demostración o simulación) fácilmente ampliable y modificable de un producto, servicio o sistema.

¿Por qué son importantes?

Los prototipos, al ser representaciones, sirven como una herramienta para analizar cómo interactúa tu público objetivo o UX Persona con el producto o servicio.

Vas a poder saber si realmente el producto o servicio cubre las necesidades halladas y si se entienden sus características y funcionalidades.

Un prototipo nos permite **explorar**, **comunicar** y **evaluar** nuestras ideas.

¿Qué beneficios tiene?

- Nos sirve para validar hipótesis, aprender y generar nuevas preguntas.
- Para equivocarnos lo antes posible y lo más barato posible.

> Fail fast, fail often

### Tipos de prototipos

En baja fidelidad: con lápiz y papel

En fidelidad media: ya hay textos definitivos. Se puede testear. Ya hay una estructura y una finalidad muy cercana al producto final. Aquí termina el trabajo del diseñador UX. Se suele realizar con apps destinadas a este propósito.

En fidelidad alta: se utilizan colores, imágenes, sombras... y otros elementos de diseño.

#### Prototipos en baja fidelidad

Es una manera rápida, barata y efectiva de evaluar y validar. Sólo hace falta lápiz y papel.

¿Cuándo debería usar esta técnica?

- Cuando no se dispone de todavía de la interfaz real.
- Cuando se dispone de poco tiempo y dinero.
- Cuando se le quiere dar mayor interés a la arquitectura y navegación en general que a los detalles.

Hay dos PDFs en la carpeta del Curso para imprimir para hacer prototipos en baja fidelidad para patrones en apps para mobile.

#### Prototipos en fidelidad media (Wireframes)

Un wireframe no es más que un boceto donde se representa visualmente, de una forma muy sencilla y esquemática, la estructura de una página web, aplicación móvil, etc.

¿Qué hace falta?

Un ordenador y un software de diseño:

- Figma.
- Sketch.
- Adobe XD.
- Axure.
-  Invision.
- Ilustrator.
- etc.

![Wireframing](img/wireframing.png)¿Qué ventajas tienen?

- Son rápidos y baratos.
- Permiten detectar y corregir los problemas antes.
- Mejoras generales.
- Mejorar la usabilidad.

¿Qué particularidades tienen?

En general carecen de características de detalle como el estilo tipográfico, el color o los gráficos. En casi todos los prototipos la atención se centra principalmente en la **funcionalidad**, el **comportamiento**, y la **disposición** de los contenidos.

**Proceso para crear un prototipo en fidelidad media**

1. Fidelidad baja: se establecen los tamaños, la rejilla y se preparan la estructura y la jerarquía.

2. Fidelidad media media baja: se organizan los bloques básicos y un título que refleje qué irá ahí y cuánto va a ocupar ese contenido en el sitio web/app.

   ![Fidelidad media media baja](img/fidelidad-media-2.png)

3. Fidelidad media baja: se completan aún más la navegación del sitio, la estructura y el contenido que va a tener.

   ![Fidelidad media baja](img/fidelidad-media-3.png)

4. Fidelidad media: es el prototipo que representa mejor el producto final aunque no contenga elementos de diseño como colores, tipografías, imágenes, etc. Se incluyen los textos, la estructura y las proporciones reales.

   ![Fidelidad media](img/fidelidad-media-4.png)

## Diseño UI

### Forma y función

![Forma y función](img/forma-funcion.png)

![Forma y función](img/forma-funcion-2.png)

![Diseño funcional/Diseño estético](img/forma-funcion-3.png)

### Usabilidad en UI

> "El 75 % de los problemas de usabilidad de una interfaz están relacionados con un mal diseño de Arquitectura de Información".
>
> Jakob Nielsen

El otro 25 % recae en problemas de usabilidad UI.

¿Qué procesa el usuario cuando navega una interfaz?

**Interpretar palabras** → Usabilidad verbal. Es la propiedad del lenguaje verbal de evidenciar la comprensión del uso del componente.

**Comprender componentes de interacción** → *Affordances* y *signifiers*. Affordance es la propiedad de un elemento de evidenciar visualmente la comprensión de su uso (Don Norman, 1988). Los affordances muestran cuáles son las posibles acciones, mientras que los signifiers te ayudan a descubrir estas posibles acciones. Un cartel de Pull/Push es una puerta es un signifier.

![Affordance/Signifier](img/affordance-signifier.png)

**Interpretar imágenes** → Comunicación visual

### Evaluación heurística

Es un proceso en el que los diseñadores utilizan reglas para medir la usabilidad de las interfaces en pantallas independientes e informar problemas.

En el diseño de la experiencia del usuario (UX), los diseñadores utilizan la evaluación heurística para determinar la usabilidad de un diseño/producto.

Revisan una lista de verificación de criterios para encontrar fallos actuales si el producto/servicio ya existe, o de un nuevo diseño.

**Los 10 principios**:

1. **Visibilidad del Contexto**. La interfaz debe mostrar a los usuarios dónde se encuentran y de dónde vienen. Debe ser evidente si se mantienen dentro, o si pasaron a otra aplicación. Breadcrumbs, tabs, pasos (stepper)
2. **Coincidencia entre el sistema y el mundo real.** La interfaz deberá expresarse en el lenguaje del usuario, con palabras, frases y conceptos que le sean familiares, evitando no hacerlo con términos propios del sistema informático. Seguir las convenciones del mundo real, desplegando la información en un orden natural y lógico. Para eso debemos tener en cuenta el *plain language* (lenguaje claro).
3. **Libertad y Control**. La interfaz debe imponer la menor cantidad posible de restricciones a los usuarios, permitiéndoles elegir los caminos y las formas de cumplir sus objetivos. Funciones tales como deshacer, actualizar o rehacer. El elemento más usado de un navegador es la flecha de página anterior.
4. **Consistencia y Estándares**. Los usuarios no deben cuestionarse si acciones, situaciones o palabras diferentes significan en realidad la misma cosa. Hay que seguir las convenciones establecidas.
5. **Prevenir los errores**. Es mejor un diseño cuidadoso que anticipa y previene los problemas que buenos mensajes de error. O se eliminan las condiciones que conducen al error, o se chequean y se advierte al usuario antes de que confirme la acción. Hay que dar opción de deshacer y hacer.
6. **Reconocer es mejor que recordar**. Minimizar la carga en la memoria del usuario haciendo los objetos, acciones y opciones visibles y reconocibles. Utilizar recusos estandarizados (por ejemplo, los típicos iconos que se usan siempre para una acción o función). El usuario no debe tener necesidad de recordar información de un modal o de una página anterior.
7. **Flexibilidad y eficiencia de uso**. La interfaz debe estar optimizada para minimizar el esfuerzo que requiere al usuario alcanzar sus objetivos. No solicitar jamás información innecesaria, acortar al mínimo los formularios y procesos.
8. **Diseño minimalista y estético**. Las páginas no deben contener información que sea irrelevante o remotamente necesaria. Cada elemento o unidad extra de información compite con las unidades relevantes de información y reduce, por tanto, su visibilidad relativa.
9. **Ayudar a reconocer, diagnosticar y recuperarse de los errores**. Los mensajes de error deben expresarse en un lenguaje sencillo (sin códigos de error o jerga informática), indicar con precisión el problema y sugerir una solución de manera constructiva. Usar imágenes de mensajes de error tradicionales, como texto rojo en negrita. Indicar a los usuarios lo que salió mal en un lenguaje que puedan entender. Evitar el lenguaje técnico. Ofrecer a los usuarios una solución, o u un atajo que pueda resolver el error de inmediato.
10. **Ayuda y documentación**. Es mejor si el sistema no necesita ninguna explicación adicional. Sin embargo, puede ser necesario proporcionar documentación para ayudar a los usuarios a comprender cómo completar sus tareas. Documentación de ayuda fácil de buscar. Siempre que sea posible presentar la documentación en contexto justo en el momento en que el usuario la necesite. Enumerar los pasos concretos a realizar.

#### Cómo realizar una evaluación heurística

**Pasos**:

1. Saber qué probar y cómo.

2. Conocer a los usuarios y sus objetivos.

3. Buscar más diseñadores (de 3 a 5).

4. Definir las heurísticas a utilizar.

5. Informarse todos respecto a qué área cubrir (si desde problemas más críticos a menos críticos).

6. Navegar el producto/servicio libremente para identificar el flujo.

7. Comenzar evaluando por pantallas y/o elementos individuales.

8. Analizar la información recopilada y sugerir soluciones.

**Ventajas de la investigación heurística**:

- No contempla las cuestiones/problemas éticos y prácticos que tienen los métodos que involucran a usuarios reales.

- Las heurísticas pueden ayudarnos a centrar nuestra atención en ciertos temas de gran relevancia.

- Evaluar los diseños utilizando un conjunto de heurísticas puede ayudar a identificar problemas de usabilidad con elementos individuales y ver cómo afectan a la experiencia general del usuario.

**Desventajas de la investigación heurística**:

- La evaluación heurística puede llevar relativamente mucho tiempo, en comparación con otros métodos de inspección *Fast & Dirty*, como los recorridos simples de una pequeña prueba de usabilidad con usuarios.

- La capacitación de los diseñadores que la llevarán a cabo necesita alrededor de una semana, sin incluir el tiempo que lleva realizar las evaluaciones y las sesiones de priorización, etc.

### Ley de Fitts

Permite calcular el tiempo que un usuario necesita para calcular un objetivo en función del tamaño y la distancia del blanco. Es un factor clave de la usabilidad web.

> "El tiempo necesario para alcanzar un objetivo con un movimiento rápido es una función del tamaño de dicho objetivo y de la distancia que hay que recorrer hasta él".
> 
>Paul Fitts, 1954

**a + b * log^2^ (D/W + 1)**

En UX el tamaño **sí** importa.

Las acciones de mayor relevancia en una web deben estar lo más cerca posible de la posición de la que parta el cursor.

Además, la superficie sobre la que se puede hacer clic debe ser bastante más grande que el tamaño del cursor.

![Distancias en mobile](img/distancias-mobile.png)

Diseñar teniendo en cuenta el movimiento natural.

### El uso del color en UI

Los colores pueden ayudarnos a construir identidad visual.
Pero cuidado, los colores también tienen significados culturales y una mala codificación puede perjudicar la usabilidad del producto.

![El color en UI](img/colores-ui.png)

Usar bien el color puede ayudarnos a guiar al usuario hacia su objetivo y comunicar mejor el sentido de las interacciones.

### Branding vs Usabilidad

El branding se puede definir como el proceso de construcción de una marca.

Se busca dar peso y mostrar todas las cualidades que tiene la marca.

Una marca es mucho más que un nombre y un logotipo, es crear un ecosistema complejo donde todo esté interconectado. Lo que la marca transmite, evoca y comunica a los clientes y las personas.

![Branding](img/branding.png)

![Rebranding](img/branding-2.png)

Si no suma, resta.

Las cuestiones corporativas de branding no deberían interferir seriamente en la usabilidad.

### Diseño emocional

Las interfaces de usuario deben comunicar los valores de la marca y reforzar la confianza de los usuarios.

El buen diseño es un diseño emocional.

>"Los objetos que nos resultan atractivos funcionan, de hecho, mucho mejor. No nos limitamos a usar un producto, sino que establecemos una relación emocional con él. Cuando un producto es, en términos estéticos, agradable y, además, halaga las ideas que tenemos de nosotros mismos y la sociedad, lo que experimentamos es positivo".
>
>Don Norman - *Emotional Design*

#### Respuestas cognitivas

Tres niveles de respuestas cognitivas:

![Respuestas cognitivas](img/respuestas-cognitivas.png)

**Visceral**:

Apariencia de algo, aceptación o rechazo.

Las reacciones instintivas de los usuarios o sus primeras impresiones de algo. Por ejemplo: una interfaz de usuario ordenada sugiere facilidad de uso.

**Comportamiento**:

Los usuarios evalúan inconscientemente cómo *x* cosa les ayuda a alcanzar sus metas y con qué facilidad. Deben sentirse satisfechos
de tener el control con el mínimo esfuerzo requerido.

**Reflexivo**:

Habla sobre la autoimagen, satisfacción personal y recuerdos. Involucra lo emotivo, el orgullo, la satisfacción y la carga de sensaciones, e incluso los valores que una persona otorga a esto.

Después de utilizar *x* cosa, los usuarios juzgarán conscientemente su rendimiento y beneficios, incluida la relación calidad-precio.

Si están felices, seguirán usándolo, formarán vínculos emocionales con él y se lo dirán a sus amigos.

![Diseño y respuestas cognitivas](img/respuestas-cognitivas-2.png)

### Arte y diseño

El arte se interpreta, mientras que el diseño se entiende.

Diseñar es resolver problemas.

El diseño es funcional y no debe estar sujeto a libre interpretación.

### Personalidad

¿Cómo transmitir personalidad?:

- Estilo de diseño.
- Colores.
- Tipografias.
- Imágenes.
- Ilustraciones.
- Textos (*Copys*).
- Animaciones.
- Sonidos.

## Diseño visual

Tiene como objetivo mejorar el atractivo estético y la usabilidad de un diseño/producto con imágenes, tipografía, espacio, diseño y color.

El diseño visual es más que estética.

¿Por qué el diseño visual es más que la belleza del producto?

La siguiente cita lo aclara:

>"Los problemas con el diseño visual pueden desanimar a los usuarios tan rápidamente que nunca descubren todas las decisiones inteligentes que se tomaron con el diseño de navegación o interacción".
>
>Jesse James Garrett, diseñador de UX y cofundador de Adaptive Path

![Diseños](img/diseños.png)

Diseño Visual:

- Nociones de color y psicología del color.
- Grillas, Retículas y Cuadrícula.
- Tipografía.
- Gestalt.
- Composición y Balance.
- Contraste.
- Jerarquías.
- Espacio en blanco.

Una charla en TED de Don Norman:

[3 ways good design makes you happy](https://www.ted.com/talks/don_norman_3_ways_good_design_makes_you_happy)

### Colores y círculo cromático

**¿Qué es el círculo cromático?**:

El círculo cromático es un instrumento en el que los colores se organizan y segmentan circularmente en base a su tono o matiz.

Te ayuda a identificar combinaciones de colores *correctas* a partir de un modelo visual de fácil uso y comprensión.

El círculo cromático está compuesto por 3 colores primarios:

- Rojo.
- Azul.
- Amarillo.

Estos colores primarios dan origen al resto de colores y no se pueden componer a partir de la combinación de otros.

Hay colores secundarios:

- Verde.
- Naranja.
- Violeta.

Formados con la combinación de 2 colores primarios.

Y colores terciarios:

- Naranja rojizo.
- Amarillo anaranjado. 
- Verde amarillento. 
- Verde azulado.
- Azul violáceo.
- Violeta rojizo.

Formados por un primario y un secundario adyacente (el de al lado).

Colores complementarios

![Colores complementarios](img/colores-complementarios.png)


Colores en triada:

![Colores en triada](img/colores-triada.png)

Colores análogos:

![Colores análogos](img/colores-analogos.png)

Colores monocromáticos:

![Colores monocromáticos](img/colores-monocromaticos.png)

Combinación de colores tétradas o tetrádicas:

![Colores tétradas](img/colores-tetradas.png)

### Psicología del color

Los colores impactan sobre nuestras emociones.

La psicología del color es un campo de estudio que está dirigido a analizar el efecto del color en la percepción y la conducta humana.

El color tiene la capacidad de estimular o deprimir, de crear alegría o tristeza y de despertar actitudes pasivas o activas

Algunos colores nos producen una sensación de serenidad y calma, mientras que otros nos inducen ira y nos hacen sentir incómodos.

**El Rojo transmite**:

- Dinamismo.
- Calidez.
- Agresividad.
- Pasión.
- Energía.
- Peligro.

**El Amarillo transmite**:

- Calidez.
- Amabilidad.
- Positividad.
- Estimulante.
- Alegría.
- Luminosidad.

**El Naranja transmite**:

- Innovación.
- Modernidad.
- Juventud.
- Diversión.
- Accesibilidad.
- Vitalidad.

**El Verde transmite**:

- Naturaleza.
- Ética.
- Crecimiento.
- Frescura.
- Serenidad.
- Orgánico.

**El Azul transmite**:

- Profesionalismo.
- Seriedad.
- Integridad.
- Sinceridad.
- Calma.
- Infinitud.

**El Violeta transmite**:

- Lujo.
- Realeza.
- Sabiduría.
- Dignidad.
- Misterio.
- Magia.
- Espiritualidad.

**El Rosa transmite**:

- Diversión.
- Amabilidad.
- Inocencia.
- Femineidad.
- Delicadeza.
- Romanticismo.

**El Marrón transmite**:

- Masculinidad.
- Rural.
- Natural.
- Tierra.
- Simplicidad.
- Rústico.

**El Gris transmite**:

- Autoridad.
- Trayectoria.
- Sencillez.
- Respeto.
- Neutralidad.
- Humildad.

**El Negro transmite**:

- Poder.
- Sofisticación.
- Prestigio.
- Valor.
- Muerte.
- Elegancia.

**El Blanco transmite**:

- Virtuosidad.
- Pureza.
- Juventud.
- Esterilidad.
- Limpieza.
- Claridad.

### Tipografía

Se define como el arte y la técnica de organizar tipos para que el lenguaje escrito sea efectivo a la hora de ser impreso. 

Esta gestión implica seleccionar una fuente, organizar los caracteres y repartir el espacio.

#### Fuentes Serif

Las fuentes con serifa tienen su origen en los primeros años de la escritura, en la época en la que los textos se cincelaban sobre roca.

El uso de herramientas tan bastas como el cincel y el martillo hacía realmente difícil que la terminación de las diferentes letras fueran rectas, por lo que se desarrolló una técnica que daba como resultado unos adornos finales característicos conocidos como serifas.

![Fuentes Serif](img/serif.png)

El texto con serifa ayuda a que el ojo siga el texto con mayor facilidad.

Estas fuentes se asocian a valores más tradicionales. Son más serias, más académicas.

¿Qué transmiten?

Seriedad, respeto, tradición, corporativismo.

¿Para qué son apropiadas?

Para editoriales, textos largos, marcas serias y/o con trayectoria.

Ejemplos: Bodoni, Palatino, Rotis Serif, Garamond.

#### Fuentes Sans Serif

Las fuentes Sans Serif empiezan a aparecer a principios del siglo XIX en Inglaterra. Pero es en el siglo XX cuando ganan un espacio definitivo en el diseño gráfico.

Las tipografías Sans Serif responden a las necesidades de una época. La Revolución Industrial y la necesidad de nuevas formas de comunicar este cambio fomentaron el crecimiento de esta tipografía.

El desarrollo de las tipografías Sans Serif avanzó y encontró un espacio idóneo para su utilización en las pantallas (televisión, ordenador, móvil, tabletas) y en textos impresos cortos o pequeños.

![Fuentes Sans Serif](img/sans-serif.png)

¿Qué transmiten?

Modernidad, seguridad, alegría, neutralidad, minimalismo.

¿Para qué son apropiadas?

Para carteles, títulos, páginas web, aplicaciones móviles, etc. Dan descanso al ojo, son legibles y de fácil lectura.

Ejemplos: Helvetica, Arual, Futura, Gill Sans.

#### Recomendaciones

La tarea del tipógrafo es lograr que el texto cumpla con tres características esenciales.

El texto debe ser:
 - Legible.
- Estético.
- Comprensible.

Cuando hay mucho contenido textual es mejor alinear a la izquierda.

El tamaño recomendado y mínimo legible en interfaces es de 16px.
No quiere decir que no se utilice 10px, tanto en páginas web como en aplicaciones móviles.

El texto debe ser con contraste, no se puede utilizar un gris claro que no contrasta bien con el fondo blanco.

La fuente en negrita hay que usarla moderadamente, porque en gran cantidad cansa la vista.

La correcta jerarquía de los textos permite una mejor comprensión. Los encabezados son vitales.

El interlineado debe ser correcto. Ni mucho ni poco espacio en blanco.

### Espacio en blanco

El espacio en blanco es el área entre los elementos de diseño.

También es el espacio dentro de los elementos de diseño individuales, incluido el espacio entre los glifos tipográficos
(caracteres legibles).

**Espacio en blanco micro**:

![Espacio en blanco micro](img/espacio-blanco-micro.png)

**Espacio en blanco macro**:

![Espacio en blanco macro](img/espacio-blanco-macro.png)

¿Qué determina qué espacio en blanco se debe usar?

El uso de espacios macro y micro depende del siguiente factor:

**El contenido**

Cuanta mayor información en el diseño estarán disponibles menos cantidades de espacio en blanco macro. Por el contrario, aumentará el volumen de micro espacios en blanco.

-----

## Fase Evaluar

### Herramientas de medición

¿Para qué medimos?

Para descubrir si las soluciones que desarrollamos ayudan tanto al usuario como al negocio. Recordemos que los datos nos ayudan a tomar decisiones (Data-Driven Design).

**Herramientas**:

- Analytics.
- Mapas de calor. 

### Analytics

Guardan toda la información recopilada de los usuarios que interactuaron o interactúan con el producto para luego poder analizarla.

Ejemplos: Google Analytics, Semrush, Adobe Analytics.

### Mapas de calor

Un mapa de calor es una técnica de visualización de datos que muestra la magnitud de un fenómeno como color en dos dimensiones

La variación puede ser por matiz o intensidad.

Podemos dividir en 3 tipos de mapas de calor:

- Mapa de click.
- Mapa de calor.
- Mapa de scroll.

Ejemplo: MouseFlow, Heat-map, SumoMe, Crazyegg.

### Pruebas de usabilidad

Las pruebas de usabilidad son la práctica de probar el grado de facilidad al usar un diseño con un grupo de usuarios. 

Por lo general, implica observar a los usuarios mientras intentan completar tareas y se puede realizar para diferentes tipos de diseños.

Permiten medir y dar relieve a la experiencia que tiene una persona cuando interactúa con un producto o servicio.

¿Qué se obtiene?:

- Determina si las personas pueden completar de manera exitosa el objetivo del producto.
- Evalúa qué tan bien funciona un diseño.
- Se visualiza cuánto disfrutan los usuarios utilizando dicho producto / servicio.
- Logra identificar problemas y su gravedad.
- Encuentra soluciones.

>"Se trata de captar a los clientes en el acto y proporcionar información muy relevante y contextual".
>
>Paul Maritz, director ejecutivo de Pivotal

**Testear en un usuario es 100% mejor que no testear en ninguno.**

Siempre hay que testear desde el comienzo del proyecto, para mantenerse en el camino correcto y encontrar nuevos hallazgos.

Testear una persona en la instancia principal del proyecto puede resultar mejor que testear 50 cerca del final.

¿Para qué nos sirven las pruebas de U
usabilidad?

- Para demostrar hipótesis con hechos reales.
- Para entender el comportamiento de los usuarios.
- Para evitar discusiones innecesarias con el equipo de trabajo.
- Para encontrar problemas específicos que sólo pueden ser encontrados realizando una prueba.
- Para ver si estamos en el camino correcto.

#### Método Tradicional / Método Nielsen

![Método Tradicional/Método Nielsen](img/tradicional-nielsen.png)

#### Reclutar participantes

Es bueno testear con participantes que es más probable que vayan a usar tu sitio, pero la verdad es que no es tan importante como creerías.

¿A quién reclutar?

Se puede reclutar a cualquier persona, siempre y cuando cumpla los siguientes requisitos:

- No conocer ni estar involucrados en el proceso de diseño del producto a evaluar.
- No tener excesiva confianza con quien facilita u observa.
- No tener conocimientos específicos en testing. Evitar reclutar a diseñadores o profesionales de las áreas: Psicología, Sociología, RRHH, Relaciones del Trabajo, Comunicación Social, etc.

¿Cómo reclutar?:

- Establecer y comunicar el propósito de la convocatoria.
- Definir y comunicar la duración máxima de la prueba.
- Definir y comunicar la compensación (económica, regalo, premio, etc).
- Definir y comunicar el lugar, la fecha y horario.

#### Partes de una prueba de usabilidad

- Bienvenida (4 minutos). Comenzamos explicando cómo va a ser el test así el participante sabe qué esperar.
- Preguntas (2 minutos). Se realizan algunas preguntas sobre él.
- Tour por la home page (3-5 minutos). Recorrido por la home.
- Las tareas (20 a 35 minutos). Ver al participante intentar una serie de tareas prefijadas.
- Conclusiones (5 minutos). Preguntas y dudas de los observadores
y del facilitador.
- Cierre (3 minutos). Incentivo y agradecimiento.

#### ¿Quién acompaña a la persona (usuario)?

La persona que se siente al lado del participante y lleve a cabo la prueba de usabilidad se llama **facilitador**.

Cualquier persona puede hacer de facilitador.

El facilitador tiene que ser paciente, calmado y empático y sobre todo que escuche bien a la persona.

El poder ver lo que la persona hace y escuchar lo que la persona piensa es la mejor manera para que el facilitador se adentre en la persona y entender por qué algunas cosas son obvias para el usuario y otras son confusas o frustrantes para los usuarios.

¿Quién va a observar?

Todas las personas posibles (en caso de que se comparta la pantalla y estén en otro cuarto).

Si las personas que van a observar se encuentran en la misma habitación que la persona y el facilitador, no se recomienda que sean mas de 2 observadores, para no incomodar ni distraer a la persona.

##### El facilitador

Hay que tratar de ser *invisibles*.

El 99% del lenguaje verbal debe ser expresado por el usuario. El facilitador no debe hablar más que lo justo y necesario.

¿Cuándo puede intervenir el facilitador?

Existen momentos en los cuales el facilitador debe intervenir en el transcurso de la prueba:

- Cuando el usuario hace un comentario puntual sobre la interfaz.
- Cuando el usuario hace una pregunta al facilitador (depende cuál).
- Cuando el usuario interrumpe el flujo de la tarea.

**Eco**

Repite la última frase que dijo el usuario.

Usuario: *"esto es raro..."*

Facilitador: *"¿En qué sentido te parece raro?"*

**Búmeran**

Devuelve la pregunta.

Usuario: *"¿Me tengo que registrar?"*

Facilitador: *"¿Qué te parece que deberías hacer para continuar?"*

Si facilitas, no tomes notas. Si tomas notas, no facilites.

No escribas todo lo que pasa y lo que dice el participante. Si crees que puedes perderte algo importante graba la sesión para escucharla después.

No tomes notas de información que no respondan a tu pregunta de investigación y/o que no vayas a dar importancia. Te va a hacer perder tiempo en el momento del análisis.

##### El observador

Durante la observación, los observadores deben anotar al menos 3 errores de usabilidad que encontraron. Siempre definiendo el problema brevemente.

Preparación del observador:
- Apaga tu celular.
- No entres ni salgas de la sala una vez comenzado el test.

Una vez comenzado el test...:

- Mantén silencio durante toda la ejecución.
- No distraigas al participante con gestos ni movimientos.
- No dialogues con el participante.
- Anota las dudas que te surjan durante el test.

Finalizadas las tareas y post test:

- Comparte las notas con los involucrados en el proyecto para realizar el análisis en común.
- Observa como mínimo 2 o 3 participantes antes de sacar conclusiones.

#### Las tareas

¿Qué testear y cuándo hacerlo?

Nunca es demasiado temprano para testear.

Incluso si ni siquiera tienes hecho el prototipo o sitio web, testea en los competidores directos, o sitios con un mismo estilo u organización, o funcionalidades que vas a utilizar.

¿Cómo elegir las tareas?

Las tareas que puedas realizar van a estar ligadas a lo que tengas que mostrar para testear.

Ejemplo:

Pantalla de Login.

Las tareas pueden ser:

1. Crear una cuenta.
2. Ingresar a una cuenta existente.
3. Olvidé mi contraseña.
4. Olvidé mi nombre de usuario.
5. Cambiar la pregunta de seguridad.

Elige tareas suficientes para que al menos dure 30 minutos la prueba.

Entre 5 a 8 tareas, dependiendo la duración de las mismas, máximo una hora.

Estas tareas debemos redactarlas después para que los participantes entiendan exactamente qué es lo que quieres que hagan. Ofrecer un relato con contexto. Por ejemplo: *"compra un libro que quieras comprar, o compra de nuevo un libro que compraste recientemente"*.

Incluye también la información que van a necesitar, si quieres testear una cuenta existente de un mail, dales la cuenta y contraseña.

#### Guión del facilitador

Lo que decir antes de plantear las tareas a la persona.

Ejemplo:

Hola ¿Cómo estás? me llamo... y te voy a acompañar en esta sesión.

Antes de comenzar, tengo información para ti y te la voy a leer para estar seguro de que menciono todo.

Posiblemente sepas, porque te preguntamos, sobre si podías venir hoy, pero déjame que te recuerde brevemente. 

Estamos testeando un sitio web en el que estamos trabajando y queremos ver cómo se manejan las personas en él. 

Esta sesión puede llevar (entre 40 minutos a 1 hora).

Quiero comentarte que lo que estamos evaluando y testeando es este sitio, no a ti. Aquí no haces nada mal. De hecho, este es el momento y lugar donde no tienes de qué preocuparte sobre cometer errores, tener dudas y demás.

Queremos escuchar todo lo que piensas, así que por favor no tengas miedo en criticar lo que consideres. Queremos mejorarlo, y por eso necesitamos tu opinión mas sincera.

Mientras vas haciendo las cosas, te voy a pedir que pienses en voz alta, que digas todo lo que se te cruza por la mente, eso nos va a ayudar un montón.

Si tienes preguntas, hazlas. A lo mejor no te puedo responder a algunas preguntas, porque estamos interesados en ver cómo lo hacen las personas cuando no tienen a alguien sentado al lado para que las ayude. Pero voy a tratar de responder las preguntas que tengas cuando terminemos.

Y si quieres hacer un descanso en cualquier momento, házmelo saber.

Te quiero comentar que está activo el micrófono. Con tu permiso, queremos grabar todo lo que pasa en la pantalla y lo que vas diciendo. La grabación va a ser usada únicamente para ayudarnos a mejorar el sitio y no va a ser oída o vista por nadie, excepto por las personas que trabajan en el proyecto. Incluso me ayuda a mí, porque me facilita no estar tomando tantas notas mientras lo realizas.

#### Preguntas al usuario antes de realizar las tareas

Es realmente útil empezar con algunas preguntas para conocer a la persona y cómo utilizan Internet.

Te da la oportunidad de que la persona se explaye y que demuestres que eres empático oyéndolo y que lo vas a escuchar atentamente. Y sobre todo que no hay respuestas correctas o incorrectas.

Ejemplo: 

-Facilitador: primero, ¿cuál es tu ocupación?
-Participante: soy actuario
-Facilitador: nunca lo había escuchado ¿qué haces exactamente en tu trabajo?
-Participante: verifico la información y las analíticas de la empresa. Somos una empresa multinacional, por lo que hay mucho trabajo.
-Facilitador: entiendo, ahora... ¿cuántas horas a la semana le dedicas a Internet? Incluyendo navegar por distintas webs, email... Un tiempo aproximando solamente, dime.
-Participante: ah, no lo sé, posiblemente alrededor de 4 horas en el trabajo y unas 8 horas a la semana en mi casa. Mayormente los fines de semana, porque termino muy cansado los días de trabajo.
-Facilitador: ¿qué tipo de sitios sueles frecuentar o te gustan?
-Participante: lorem ipsum...
-Facilitador: ¿tienes algun sitio web favorito?
-Participante: lorem ipsum...
-Facilitador: Estupendo, con esto terminamos las preguntas. Podemos comenzar con lo demás.

#### Home Tour (Recorrido por la Home)

Ejemplo:

Primero quiero preguntarte acerca de todo lo que observes en esta página y me digas algunas cosas..

- ¿Qué piensas de la página?
- De quién crees que es este sitio?
- ¿Qué puedes hacer en él y para qué es?

Simplemente recórrela y cuéntame todo eso en voz alta. Haz scroll todo lo que quieras, pero no hagas clic en nada todavía.
#### Decidir qué arreglar

Después de haber realizado tests.

Arreglar siempre los problemas mayores.

¿Cómo hacer esto?

Primer Paso:

Realizar una lista en conjunto con todo lo que hayas observado y también los observadores. Escribidlos todos en una hoja.

Segundo Paso:

Elegir los `x` problemas más serios. Se puede realizar una votación informal, o bien ser en los que más coincidan. También en los que el usuario tuvo más problemas (si es que los tuvo).

Tercer Paso:

Puntúalos. De esos `x` seleccionados, enumera en orden de gravedad. Donde el primero será el más leve (con la menor puntuaciónn) y el `x-n` el más grave (con la mayor puntuación).

Cuarto Paso:

Y por último creá una lista de ellos. Comenzar desde el más grave y escribir las ideas que tengas para poder solucionarlos y ocuparte de ello el próximo mes. También, quién lo va a hacer y qué recursos van a hacer falta.

![Qué mejorar](img/mejorar.png)

#### Alternativas de las pruebas de usabilidad

El testing presencial es el que se ha descrito hasta ahora. Pero tambien existe la posibilidad del **testing remoto**.

Si nos referimos al testing remoto, hay dos tipos:

- Testing remoto asistido. En lugar de que la persona vaya a tu oficina, casa o donde testees a los participantes, el test se va a hacer desde el confort de la casa del participante. Mediante Zoom o Google Meet, por ejemplo. La persona comparte pantalla y audio y así se observa su comportamiento. Los condicionantes son que depende de una buena conexión a Internet, de que la persona tenga ordenador, y que sea un usuario bastante digitalizado. 
- Testing remoto no asistido. https://maze.co/ nos permite grabar la prueba de usabilidad, la pantalla, los clicks. Nos genera un mapa de calor donde uno puede colocar las tareas, y las personas simplemente las realizan. Colocas el link a tu sitio o el prototipo, ingresas las tareas y ya está. Puedes mandar el link de la prueba a tantas personas como quieras. Los condicionantes son que en esta prueba recopilas datos cuantitativos, datos duros, el tiempo que a persona tardó, la cantidad de clicks, etc. No se reciben datos cualitativos de por qué tocó lo que tocó, por qué fue a cierto lugar si era otro, por qué tardó tanto tiempo, etc.

#### Problemas típicos en las pruebas de usabilidad

**Los usuarios no entienden el concepto**. Miran la página o sección en donde están y no saben qué hacer o qué pensar. Quizás piensan que lo están haciendo mal.

**Las palabras que están buscando no están ahí**. Significa que fallaste en anticiparle a la persona lo que tiene que buscar o hacer y que las palabras y el guión que hiciste para describir las situaciones y demás no lo entienden.

**Resulta abrumador y hay muchas cosas pasando**. A veces lo que están buscando está ahí mismo en la página, pero no lo ven.

#### Razones por las que las personas no testean

- **No tengo tiempo**. Hacer las pruebas lo más sencillas posible.

- **No tengo dinero**. El método Nielsen es mucho más barato.

- **No tengo el conocimiento**. En estas pruebas sienten se obtienen hallazgos útiles.

- **No tengo oficina, no tengo sitio**. Siempre hay alternativas, pruebas en remoto...

## Kick-off

Kick-off, reunión inicial, primera reunión o reunión de lanzamiento, en el entorno empresarial, es el encuentro inicial entre el jefe de un proyecto y el cliente para el que se trabaja.

**Hay que oír**

Debemos ser capaces de escuchar y comprender sus problemas motivaciones, aspiraciones, deseos, experiencia, etc. Qué es lo que necesita.

**Cosas a evitar**

Criticar, pensar en soluciones/ideas, interrumpir, hacer juicios de valor, o intentar mencionar cómo tratarías el problema.

En definitivas cuentas, un 80% oír y un 20% preguntar.

**Motivos y objetivo - Preguntas de Discovery**

- ¿Cuál es el objetivo?
- ¿Por qué focalizaremos en él?
- ¿Cuál es el sentido del proyecto o tarea?
- ¿Qué decisiones quieres tomar con los resultados?
- ¿Se intentó anteriormente realizar este proyecto o tarea?
- ¿Qué investigación, resultados hubo?
- ¿Qué pasa si el proyecto fracasa?
- ¿Cuánto tiempo tenemos?

**Medir el éxito - Preguntas de Discovery**

- ¿Cómo van a definir el éxito?
- ¿Cuál es el resultado ideal para ustedes?

**Usuarios - Preguntas de Discovery**

- ¿Quiénes son los usuarios/consumidores?
- ¿Qué problemas estamos tratando de solucionar para ellos?
- ¿Estamos focalizando en nuevos consumidores o consumidores existentes? ¿O ambos?
- ¿Hay que considerar varias culturas?
- ¿Qué comportamientos queremos que el usuario adopte o modifique?

**Accesibilidad - Preguntas de Discovery**

- ¿Qué tareas son críticas para hacer?
- ¿Qué dispositivos y versiones debería soportar? (Móvil, tablet, laptop, desktop, TV, etc).
- ¿Qué hardware debería soportar? (ratón, teclado, braille, cámara, etc).

**Producto - Preguntas de Discovery**

- ¿Existe un manual de marca?
- ¿Tienen un Design System/UI Kit?
- ¿Con qué software trabajan?
- ¿Cómo es el traspaso de diseño a desarrollo?
- ¿Tienen documentación del diseño actual?

**Investigación - Preguntas de Discovery**

- ¿Qué clase de preguntas necesitáis saber?
- ¿Tenéis preguntas adicionales para sumar a la investigación?
- ¿Necesitamos datos cuantitativos o cualitativos?
- ¿Cómo serán los tiempos y el presupuesto?
- ¿Cómo verificamos la veracidad de la información? ¿Hace falta investigación, encuestas, entrevistas...?

**Relaciones y Equipo - Preguntas de Discovery**

- ¿Qué esperas de trabajar conmigo?
- ¿Quién toma las decisiones? ¿Con quién voy a tratar?
- ¿Quién forma parte del equipo?
- ¿Quiénes van a estar involucrados?
